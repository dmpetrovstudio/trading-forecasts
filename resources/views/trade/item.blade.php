<h3>График и описание {{ $item->name }} ({{ $item->trade->name }})</h3>
<div class="tabs">
	<ul class="tabs-nav">
		<li><a href="{{ url('trade/'.$item->id) }}"
			@if(app('request')->input('type') <= 0)
			class="active"
			@endif>Все графики</a></li>
		<li><a href="{{ url('trade/'.$item->id) }}?type=1"
			@if(app('request')->input('type') == 1)
			class="active"
			@endif>Внутридневные</a></li>
		<li><a href="{{ url('trade/'.$item->id) }}?type=2"
			@if(app('request')->input('type')  == 2)
			class="active"
			@endif>Дневные</a></li>
		<li><a href="{{ url('trade/'.$item->id) }}?type=3"
			@if(app('request')->input('type') == 3)
			class="active"
			@endif>Неделя</a></li>
	</ul>
	<div class="tabs-wrap">
		<div id="tabs1" class="tabs-item active">
			<div class="tabs-item__body">
				<div class="row">
					<div class="col-6">
						<div class="tabs-item__chart">
							@foreach($blog as $blog_item)
							<img src="{{ url('images/blog/list/'.$blog_item->photo) }}" alt="">
							@endforeach
						</div>
					</div>
					<div class="col-6">
						<div class="tabs-item__info">
							@foreach($blog as $blog_item)
							<p><strong>{{ $blog_item->title }}</strong></p>
							<p>{!! $blog_item->text !!}</p>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
