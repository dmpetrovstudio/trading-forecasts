@extends('cabinet.cabinet')

@section('title', 'Выбор рынка')

@section('content')
<aside class="aside">
	<h4 class="hidden-md">Все рынки</h4>
	<div class="select-market__btn visible-md">Выбор рынка</div>
	<div class="accordion all-markets">
		@foreach($list as $trade)
		<div class="accordion-item">
			<div class="accordion-item__header">{{ $trade->name }}</div>
			<div class="accordion-item__body">
				<ul class="accordion-item__list">
					@foreach($trade->list as $item)
					<li><a href="{{ url('trade/'.$item->id) }}">{{ $item->name }}</a></li>
					@endforeach
				</ul>
			</div>
		</div>
		@endforeach
	</div>
</aside>
<div class="content">{!! $content !!}</div>
@endsection
