@push('scripts')
<script src="{{ asset('js/register.js') }}"></script>
@endpush

@extends('layouts.app')

@section('title', 'Регистрация')

@section('content')
<section class="registration">
	<div class="container">
		<form method="POST" action="{{ route('register') }}">
			@csrf
			<div class="registration-wrap row">
				<div class="col-7">
					<h1>Регистрация</h1>
					<div class="form registration-form">
						<div class="form-item">
							<input type="text" name="parent"  value="{{ session('referal_name') }}" disabled>
							<label>Кто пригласил</label>
						</div>
						<div class="form-item">
							<input type="text" required name="name" required value="{{ old('name') }}">
							<label>Логин*</label>
							@if ($errors->has('name'))
		              <span class="invalid-feedback">
		                  <strong>{{ $errors->first('name') }}</strong>
		              </span>
		          @endif
						</div>
						<div class="form-item">
							<input type="password" name="password" required>
							<label>Пароль*</label>
							@if ($errors->has('password'))
		              <span class="invalid-feedback">
		                  <strong>{{ $errors->first('password') }}</strong>
		              </span>
		          @endif
						</div>
						<div class="form-item">
							<input type="password" name="password_confirmation" required>
							<label>Подтвердить пароль*</label>
						</div>
						<div class="form-item">
							<input type="text" name="email" required value="{{ old('email') }}">
							<label>Email*</label>
							@if ($errors->has('email'))
		              <span class="invalid-feedback">
		                  <strong>{{ $errors->first('email') }}</strong>
		              </span>
		          @endif
						</div>
						<button type="submit" class="btn">Зарегистрироваться</button>
					</div>
				</div>
				<div class="col-5">
					<div class="registration-login">
						<h3>Уже зарегистрированы?</h3>
						<a href="{{ url('login') }}" class="btn btn-white">Войти в свой аккаунт</a>
					</div>
				</div>
			</div>
		</form>
	</div>
</section>
@endsection
