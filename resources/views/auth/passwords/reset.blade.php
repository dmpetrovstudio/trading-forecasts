@extends('layouts.app')

@section('title', 'Установить новый пароль')

@section('content')
<section class="registration">
	<div class="container">
		<div class="registration-wrap row">
			<div class="col-7">
				<h1>Установить новый пароль</h1>
				<form class="form registration-form" method="POST" action="{{ route('password.request') }}">
					@csrf
					<input type="hidden" name="token" value="{{ $token }}" />
					<div class="form-item">
						<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
						<label>Email</label>
            @error('email')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
					</div>
					<div class="form-item">
						<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
						<label>Пароль*</label>
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
					</div>
					<div class="form-item">
						<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
						<label>Повторите пароль*</label>
					</div>
					<div class="subscription-total">
						<button type="submit" class="btn">Восстановить</button>
					</div>
				</form>
			</div>
			<div class="col-5">
				<div class="registration-login">
					<h3>Еще не зарегистрированы?</h3>
					<a href="/register" class="btn btn-white">Зарегистрируйтесь</a>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
