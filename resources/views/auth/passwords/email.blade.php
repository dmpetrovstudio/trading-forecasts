@extends('layouts.app')

@section('title', 'Восстановление пароля')

@section('content')
<section class="registration">
	<div class="container">
		<div class="registration-wrap row">
			<div class="col-7">
				<h1>Восстановление пароля</h1>
				<form class="form registration-form" method="POST" action="{{ route('password.email') }}">
					@csrf
					<div class="form-item">
						<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email">
						<label>Email</label>
            @error('email')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
					</div>
					<div class="subscription-total">
						<button type="submit" class="btn">Восстановить</button>
					</div>
				</form>
			</div>
			<div class="col-5">
				<div class="registration-login">
					<h3>Еще не зарегистрированы?</h3>
					<a href="/register" class="btn btn-white">Зарегистрируйтесь</a>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
