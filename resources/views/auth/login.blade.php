@extends('layouts.app')

@section('title', 'Вход в систему')

@section('content')
<section class="registration">
	<div class="container">
		<div class="registration-wrap row">
			<div class="col-7">
				<h1>Вход</h1>
				<form class="form registration-form" method="POST" action="{{ route('login') }}">
					@csrf
					<div class="form-item">
						<input type="text" name="email">
						<label>Email или Логин</label>
						@if ($errors->has('email'))
							 <span class="invalid-feedback">
									 <strong>{{ $errors->first('email') }}</strong>
							 </span>
					 @endif
					</div>
					<div class="form-item">
						<input type="password" name="password">
						<label>Пароль*</label>
						@if ($errors->has('password'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
					</div>
					<a href="{{ url('password/reset') }}">Забыли пароль?</a>
					<br /><br />
					<div class="subscription-total">
						<button type="submit" class="btn">Войти</button>
					</div>
				</form>
			</div>
			<div class="col-5">
				<div class="registration-login">
					<h3>Еще не зарегистрированы?</h3>
					<a href="/register" class="btn btn-white">Зарегистрируйтесь</a>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
