<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>@yield('title')</title>
	<link type="image/x-icon" rel="shortcut icon" href="{{ url('favicon.ico') }}">
	<link type="text/css" rel="Stylesheet" href="{{ url('css/jquery.fancybox.min.css') }}">
	<link type="text/css" rel="Stylesheet" href="{{ url('css/slick.css') }}">
	<link type="text/css" rel="Stylesheet" href="{{ url('css/style.css') }}">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
</head>
<body>
  @include('main.header')
  @yield('content')
  <script src="{{ url('js/jquery-1.12.4.min.js') }}"></script>
  <script src="{{ url('js/jquery.fancybox.min.js') }}"></script>
  <script src="{{ url('js/jquery.scrollbar.min.js') }}"></script>
  <script src="{{ url('js/slick.min.js') }}"></script>
  <script src="{{ url('js/engine.js') }}"></script>
	@stack('scripts')
</body>
</html>
