@push('scripts')
<script src="{{ asset('js/dropzone.js') }}"></script>
<script src="{{ asset('js/profile.js') }}"></script>
@endpush

@push('styles')
<link href="{{ asset('css/dropzone.css') }}" rel="stylesheet">
<link href="{{ asset('css/profile.css') }}" rel="stylesheet">
@endpush

@extends('cabinet.cabinet')

@section('title', 'Мои подписки')

@section('content')
<aside class="aside">
	<div class="aside-profile">
		<div class="aside-profile__wrap hidden-xs">
			<form id="avatarForm" class="dropzone" action="{{ url('image/upload') }}" enctype="multipart/form-data" method="post">
				@csrf
				<input type="hidden" name="directory" value="avatar" />
				<label for="avatarForm">
					<img src="{{ url('images/avatar/cabinet/'.Auth::user()->avatar) }}" alt="">
				</label>
			</form>
			<h3>{{ Auth::user()->name }}</h3>
		</div>
		<h3 class="visible-xs">Мои подписки</h3>
		<ul class="profile-menu">
			<li><a href="{{ url('profile') }}">Информация</a></li>
			<li><a href="{{ url('referal') }}">Реферальная прог.</a></li>
			<li><a href="{{ url('profile/subscribers') }}" class="active">Мои подписки</a></li>
		</ul>
	</div>
</aside>
<div class="content">
	<h3 class="hidden-xs">Мои подписки</h3>
	<form action="{{ route('subscriber.save') }}" method="post">
		@csrf
		<table class="referral-users__table">
			<thead>
				<tr>
					<td>Автор</td>
					<td colspan="2">Рынки</td>
				</tr>
			</thead>
			<tbody>
				@foreach($authors as $author)
				<tr data-author="{{ $author->id }}">
					<td>
						<span class="referral-users__table--user">
							<a href="{{ url('profile/'.$author->id) }}">
								<img src="{{ Storage::disk('avatar')->url('thumbnail/'.$author->avatar) }}" alt="" width="40px" />
								<strong>{{ $author->name }}</strong>
							</a>
						</span>
					</td>
					<td>
						<ul>
							@foreach($trade as $trade_row)
							<li class="subscription-table__market">
								<h4>{{ $trade_row->name }}</h4>
								<ul>
									@foreach($trade_row->list as $trade_item)
									<li class="subscription-table__market">
										<input type="checkbox" id="trade{{ $trade_item->id }}" name="trade[{{ $author->id }}][{{ $trade_item->id }}]" @php if($matrix[$author->id][$trade_item->id]['checked']) echo " checked='checked'" @endphp />
										<label for="trade{{ $trade_item->id }}">
											{{ $trade_item->name }}
											<small>{{ $matrix[$author->id][$trade_item->id]['price'] }} руб.</small>
										</label>
									</li>
									@endforeach
								</ul>
							</li>
							@endforeach
						</ul>
					</td>
					<td>
						<a class="btn btn-green btn-subscriber-delete">Отписаться</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		<div id="answer-out"></div>
		<div class="subscription-total">
			<button type="submit" class="btn">Сохранить изменения</button>
		</div>
	</form>
</div>
@endsection
