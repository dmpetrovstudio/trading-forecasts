@push('scripts')
<script src="{{ asset('js/dropzone.js') }}"></script>
<script src="{{ asset('js/profile.js') }}"></script>
@endpush

@push('styles')
<link href="{{ asset('css/dropzone.css') }}" rel="stylesheet">
<link href="{{ asset('css/profile.css') }}" rel="stylesheet">
@endpush

@extends('cabinet.cabinet')

@section('title', 'Профиль')

@section('content')
<aside class="aside">
	<div class="aside-profile">
		<div class="aside-profile__wrap hidden-xs">
			<form id="avatarForm" class="dropzone" action="{{ url('image/upload') }}" enctype="multipart/form-data" method="post">
				@csrf
				<input type="hidden" name="directory" value="avatar" />
				<label for="avatarForm">
					<img src="{{ url('images/avatar/cabinet/'.Auth::user()->avatar) }}" alt="">
				</label>
			</form>
			<h3>{{ Auth::user()->name }}</h3>
		</div>
		<p>{{ Auth::user()->description }}</p>
		<div class="subscription-author__icons--item">
			<i class="icon-user"></i>
			<div class="subscription-author__icons--wrap">
				<span>Подписчики</span>
				<strong>{{ Auth::user()->countUsers() }}</strong>
			</div>
		</div>
		<div class="subscription-author__icons--item">
			<i class="icon-message"></i>
			<div class="subscription-author__icons--wrap">
				<span>Комментарии</span>
				<strong>{{ Auth::user()->countComment() }}</strong>
			</div>
		</div>
		<ul class="profile-menu">
			<li><a href="{{ url('profile-price') }}" class="btn btn-blue">Цены</a></li>
		</ul>
	</div>
</aside>
<div class="content">
	<h3 class="hidden-xs">Мой профиль</h3>
	<form class="profile-info" method="POST" action="{{ route('profile') }}">
		<div class="form">
			@csrf
			<div class="form-item">
				<input type="text" value="{{ Auth::user()->name }}" name="name">
				<label>Логин</label>
			</div>
			<div class="form-item valid">
				<input type="text" value="{{ Auth::user()->email }}" name="email">
				<label>Email</label>
			</div>
			<div class="form-item valid">
				<textarea name="description">{{ Auth::user()->description }}</textarea>
				<label>Описание</label>
			</div>
			<div class="form-item valid">
				<input type="date" value="{{ Auth::user()->birthday }}" name="birthday">
				<label>Дата рождения</label>
			</div>
			<div class="form-item valid">
				<input type="text" value="{{ Auth::user()->country }}" name="country">
				<label>Страна</label>
			</div>
			<div class="form-item valid">
				<input type="text" value="{{ Auth::user()->city }}" name="city">
				<label>Город</label>
			</div>
		</div>
		<a href="#" class="btn btn-blue reset-password-btn">Сменить пароль</a>
		<div class="form password-form" style="display: none;">
			<div class="form-item">
				<input type="password" value="" name="password">
				<label>Новый пароль</label>
			</div>
			<div class="form-item">
				<input type="password" value="" name="password2">
				<label>Повторите новый пароль</label>
			</div>
		</div>
		<button type="submit" class="btn">Сохранить изменения</button>
	</form>
</div>
@endsection
