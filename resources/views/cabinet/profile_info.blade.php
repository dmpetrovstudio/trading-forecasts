@push('scripts')
<script src="{{ asset('js/profile.js') }}"></script>
@endpush

@extends('cabinet.cabinet')

@section('title', 'Профиль')

@section('content')
<aside class="aside">
	<div class="aside-profile">
		<div class="aside-profile__wrap hidden-xs">
			<img src="{{ url('images/avatar/cabinet/'.$user->avatar) }}" alt="">
			<h3>{{ $user->name }}</h3>
		</div>
		<p>{{ Auth::user()->description }}</p>
		<ul class="profile-menu">
			<li><a class="btn btn-blue" href="{{ url('chat/new/'.$user->id) }}">Написать</a></li>
			@if($user->fk_role_id == 2)
			<li><br /></li>
			<li><a class="btn" href="{{ url('blog/author/'.$user->name) }}">Статьи</a></li>
			@endif
		</ul>
	</div>
</aside>
<div class="content">
	<h3 class="hidden-xs">Информация</h3>
	<div class="profile-info">
		<div class="form">
			<p>Логин : {{ $user->name }}</p>
			<p>Email: {{ $user->email }}</p>
		</div>
	</div>
</div>
@endsection
