@push('scripts')
<script src="{{ asset('js/dropzone.js') }}"></script>
<script src="{{ asset('js/profile.js') }}"></script>
<script src="{{ asset('js/clipboard-polyfill.js') }}"></script>
@endpush

@push('styles')
<link href="{{ asset('css/dropzone.css') }}" rel="stylesheet">
<link href="{{ asset('css/profile.css') }}" rel="stylesheet">
@endpush

@extends('cabinet.cabinet')

@section('title', 'Реферальная программа')

@section('content')
<aside class="aside">
	<div class="aside-profile">
		<div class="aside-profile__wrap hidden-xs">
			<form id="avatarForm" class="dropzone" action="{{ url('image/upload') }}" enctype="multipart/form-data" method="post">
				@csrf
				<input type="hidden" name="directory" value="avatar" />
				<label for="avatarForm">
					<img src="{{ url('images/avatar/cabinet/'.Auth::user()->avatar) }}" alt="">
				</label>
			</form>
			<h3>{{ Auth::user()->name }}</h3>
		</div>
		<h3 class="visible-xs">Мой профиль</h3>
		<ul class="profile-menu">
			<li><a href="{{ url('profile') }}">Информация</a></li>
			<li><a href="{{ url('referal') }}" class="active">Реферальная прог.</a></li>
			<li><a href="{{ url('profile/subscribers') }}">Мои подписки</a></li>
		</ul>
	</div>
</aside>
<div class="content">
	<h3>Реферальная программа</h3>
	<div class="profile-referral">
		<p>Получите что-то, поделившись этой ссылкой с другом. <br>После его регистрации, вы получите вот это.</p>
		<div class="form referral-form">
			<div class="form-item valid active">
				<input id="referal_url" type="text" value="{{ url('/ref/'.Auth::user()->referal) }}">
				<label>Реферальная ссылка</label>
			</div>
			<a href="#" class="btn btn-blue referal_copy">Скопировать</a>
		</div>
		@if($authors->count() > 0 || $pods->count() > 0)
		<div class="referral-users">
			<h3>Приглашенные вами люди</h3>
			@if($authors->count() > 0)
			<h4>Авторы</h4>
			<table class="referral-users__table">
				<thead>
					<tr>
						<td>Пользователь</td>
						<td colspan="2">Реферальный бонус</td>
					</tr>
				</thead>
				<tbody>
					@foreach($authors as $author)
					<tr>
						<td>
							<span class="referral-users__table--user">
								<img src="{{ url('data/messages-header.png') }}" alt="">
								<strong>{{ $author->name }}</strong>
							</span>
						</td>
						<td>
							<strong>10 000 ₽</strong>
						</td>
						<td>
							<button class="btn btn-green">Вывести бонусы</button>
							<?php /*<button class="btn btn-grey" disabled>Выведено</button>*/ ?>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			@endif
			@if($pods->count() > 0)
			<h4>Подписчики</h4>
			<table class="referral-users__table">
				<thead>
					<tr>
						<td>Пользователь</td>
						<td colspan="2">Реферальный бонус</td>
					</tr>
				</thead>
				<tbody>
					@foreach($pods as $pod)
					<tr>
						<td>
							<span class="referral-users__table--user">
								<img src="{{ url('data/messages-header.png') }}" alt="">
								<strong>{{ $pod->name }}</strong>
							</span>
						</td>
						<td>
							<strong>10 000 ₽</strong>
						</td>
						<td>
							<button class="btn btn-grey" disabled>Выведено</button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			@endif
		</div>
		@endif
	</div>
</div>
@endsection
