<div class="wrap wrap-user">
	<header class="header">
		<div class="header-nav">
			<a href="{{ url('') }}" class="logo"><i class="icon icon-ts"></i></a>
			<button class="burger-menu visible-md"><i class="icon-menu"></i><span>Меню</span></button>
			<ul class="user-menu">
				<li><a href="{{ url('trade') }}" class="{{ Request::is('trade', 'trade/*') ? 'active' : '' }}"><i class="icon-list"></i><span>Выбор <br> рынка</span></a></li>
				<li><a href="{{ url('blog/last') }}" class="{{ Request::is('blog/last') ? 'active' : '' }}"><i class="icon-update"></i><span>Последние <br> обновления</span></a></li>
				<li><a href="{{ url('chat') }}" class="{{ Request::is('chat', 'chat/*') ? 'active' : '' }}"><i class="icon-message-fill"></i><span>Сообщения</span></a></li>
				<!--<li><a href="{{ url('support') }}" class="{{ Request::is('support') ? 'active' : '' }}"><i class="icon-support"></i><span>Клиентская <br> поддержка</span></a></li>-->
				<li><a href="{{ url('profile') }}" class="{{ Request::is('profile', 'profile/*', 'referal') ? 'active' : '' }}"><i class="icon-settings"></i><span>Настройки</span></a></li>
				<li class="visible-md">
					<ul class="main-menu">
						<li><a href="#">Подписки</a></li>
						<li><a href="#">Обучение</a></li>
						<li><a href="#">Ресурсы</a></li>
						<li class="soc-btn__mobile">
							<ul class="soc-btn">
								<li><a href="#"><i class="icon-facebook"></i></a></li>
								<li><a href="#"><i class="icon-vk"></i></a></li>
								<li><a href="#"><i class="icon-instagram"></i></a></li>
								<li><a href="#"><i class="icon-telegram"></i></a></li>
								<li><a href="#"><i class="icon-twitter"></i></a></li>
							</ul>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="user-nav">
			<a href="{{ url('profile') }}" class="user-nav__user"><img src="{{ url('images/avatar/icon/'.Auth::user()->avatar) }}" alt=""></a>
			<a href="{{ url('logout') }}" class="user-nav__logout"><i class="icon-logout"></i></a>
		</div>
	</header>
