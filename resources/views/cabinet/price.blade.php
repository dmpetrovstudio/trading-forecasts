@push('scripts')
<script src="{{ asset('js/dropzone.js') }}"></script>
<script src="{{ asset('js/profile.js') }}"></script>
@endpush

@push('styles')
<link href="{{ asset('css/dropzone.css') }}" rel="stylesheet">
<link href="{{ asset('css/profile.css') }}" rel="stylesheet">
@endpush

@extends('cabinet.cabinet')

@section('title', 'Цены')

@section('content')
<aside class="aside">
	<div class="aside-profile">
		<div class="aside-profile__wrap hidden-xs">
			<form id="avatarForm" class="dropzone" action="{{ url('image/upload') }}" enctype="multipart/form-data" method="post">
				@csrf
				<input type="hidden" name="directory" value="avatar" />
				<label for="avatarForm">
					<img src="{{ url('images/avatar/cabinet/'.Auth::user()->avatar) }}" alt="">
				</label>
			</form>
			<h3>{{ Auth::user()->name }}</h3>
		</div>
		<p>{{ Auth::user()->description }}</p>
		<div class="subscription-author__icons--item">
			<i class="icon-user"></i>
			<div class="subscription-author__icons--wrap">
				<span>Подписчики</span>
				<strong>{{ Auth::user()->countUsers() }}</strong>
			</div>
		</div>
		<div class="subscription-author__icons--item">
			<i class="icon-message"></i>
			<div class="subscription-author__icons--wrap">
				<span>Комментарии</span>
				<strong>{{ Auth::user()->countComment() }}</strong>
			</div>
		</div>
		<ul class="profile-menu">
			<li><a href="{{ url('profile-price') }}" class="btn btn-blue active">Цены</a></li>
		</ul>
	</div>
</aside>
<div class="content">
	<h3 class="hidden-xs">Цены за подписки</h3>
	<form class="profile-info" method="POST" action="{{ route('profile.price.save') }}">
		@csrf
		<table class="referral-users__table">
			<tbody>
				@foreach($list as $item)
				<tr>
					<td>{{ $item->name }}</td>
					<td></td>
					<td></td>
				</tr>
				@foreach($item->list as $row)
				<tr>
					<td>{{ $row->name }}</td>
					<td>
						<span class="form-item valid active">
							<input type="text" name="price[{{ $row->id }}]" value="{{ $row->usersTradePrice() }}" />
							<label>Цена за подписку</label>
						</span>
					</td>
					<td>
						<span class="form-item valid active">
							<input type="text" name="frequency[{{ $row->id }}]" value="{{ $row->usersTradeFrequency() }}" />
							<label>Частота выхода</label>
						</span>
					</td>
				</tr>
				@endforeach
				@endforeach
			</tbody>
		</table>
		<button type="submit" class="btn">Сохранить изменения</button>
	</form>
</div>
@endsection
