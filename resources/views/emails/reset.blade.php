<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Сброс пароля</title>
</head>
<body>
    <h1>Вы направили запрос на сброс пароля!</h1>
    <p>
        Перейдите <a href='{{ url("/password/reset/{$token}?email={$email}") }}'>по ссылке </a>чтобы установить новый пароль!
    </p>
</body>
</html>
