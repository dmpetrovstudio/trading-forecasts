<div class="chart-day">
  <div class="container">
    <div class="row">
      <div class="col-6">
        <h3>График дня</h3>
        <h6>Elliott Wave View: EURJPY<br> возобновил снижение</h6>
        <div class="chart-day__chart">
          <img src="data/chart-1.png" alt="">
        </div>
      </div>
      <div class="col-6">
        <div class="chart-day__info">
          <p>После формирования минимума 3 сентября 2019 года на уровне 115,8 пара EURJPY выросла в три этапа. Ралли закончилось на отметке 120,62, и мы отметили этот отскок как волну 4. Поскольку структура ралли от минимума 3 сентября 2019 года состоит из 3 волн, это говорит о том, что ралли является лишь коррекцией.</p>
          <a href="#" class="btn btn-white">Читать далее</a>
        </div>
      </div>
    </div>
  </div>
</div>
