<div class="banner">
  <img src="data/banner.png" alt="">
  <div class="banner-overlay">
    <div class="container">
      <div class="banner-overlay__wrap">
        <div class="banner-title">Увеличьте успех <br>трейдера</div>
        <p>Изучайте аналитику на основе волнового принципа Эллиотта</p>
        <form action="#" class="form banner-form">
          <div class="form-item">
            <input type="text" required>
            <label>Электронная почта</label>
          </div>
          <button class="btn">Попробовать бесплатно</button>
        </form>
      </div>
    </div>
  </div>
</div>
