<footer class="footer footer-home">
  <div class="container">
    <div class="row">
      <div class="col-6">
        <h2>Научитесь <br> зарабатывать</h2>
        <h5>Запишитесь на курс обучения волновому анализу</h5>
        <a href="#" class="btn">Записаться на обучение</a>
        <nav class="footer-nav justify-align">
          <a href="#" class="footer-logo"><i class="icon-ts"></i><span>rubus <br>forecasts</span></a>
          <ul class="footer-menu">
            <li><a href="#">Подписки</a></li>
            <li><a href="#">Обучение</a></li>
            <li><a href="#">Ресурсы</a></li>
          </ul>
        </nav>
      </div>
      <div class="col-6">
        <div class="footer-img">
          <img src="data/footer-img.png" alt="">
        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <p>Все публикации на сервисе носят рекомендательный характер и не призывают к дейсвтию. Каждый действует на свой страх и риск</p>
      <ul class="soc-btn">
        <li><a href="#"><i class="icon-facebook"></i></a></li>
        <li><a href="#"><i class="icon-vk"></i></a></li>
        <li><a href="#"><i class="icon-instagram"></i></a></li>
        <li><a href="#"><i class="icon-telegram"></i></a></li>
        <li><a href="#"><i class="icon-twitter"></i></a></li>
      </ul>
    </div>
  </div>
</footer>
<!-- END of .footer -->
