<header class="header">
  <div class="container">
    <div class="header-wrap justify-align">
      <div class="header-nav">
        <a href="/" class="logo"><i class="icon icon-ts"></i><span>Tradescope</span></a>
        <button class="burger-menu visible-xs"><i class="icon-menu"></i><span>Меню</span></button>
        <ul class="main-menu">
          <li><a href="#" class="btn btn-blue visible-xs">Попробовать бесплатно</a></li>
          <li><a href="#">Подписки</a></li>
          <li><a href="#">Обучение</a></li>
          <li><a href="#">Ресурсы</a></li>
          <li class="soc-btn__mobile visible-xs">
            <ul class="soc-btn">
              <li><a href="#"><i class="icon-facebook"></i></a></li>
              <li><a href="#"><i class="icon-vk"></i></a></li>
              <li><a href="#"><i class="icon-instagram"></i></a></li>
              <li><a href="#"><i class="icon-telegram"></i></a></li>
              <li><a href="#"><i class="icon-twitter"></i></a></li>
            </ul>
          </li>
        </ul>
      </div>
      <ul class="soc-btn hidden-xs">
        <li><a href="#"><i class="icon-facebook"></i></a></li>
        <li><a href="#"><i class="icon-vk"></i></a></li>
        <li><a href="#"><i class="icon-instagram"></i></a></li>
        <li><a href="#"><i class="icon-telegram"></i></a></li>
        <li><a href="#"><i class="icon-twitter"></i></a></li>
      </ul>
      <div class="header-right">
        @if (Auth::id() > 0)
        <a href="{{ url('cabinet') }}" class="header-right__login">Личный кабинет</a>
        <a href="{{ url('logout') }}" class="header-right__login">Выйти</a>
        @else
        <a href="{{ url('login') }}" class="header-right__login">Войти</a>
        <a href="{{ url('register') }}" class="btn btn-blue hidden-md">Попробовать бесплатно</a>
        @endif
      </div>
    </div>
  </div>
</header>
<!-- END of header -->
