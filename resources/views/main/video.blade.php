<div class="latest-videos">
  <div class="container">
    <h3>Последние опубликованные <br> видео по теме трейдинга</h3>
    <div class="latest-videos__slider">
      <div class="col-4">
        <div class="latest-videos__item">
          <img src="https://via.placeholder.com/363x363" alt="">
          <div class="latest-videos__item--overlay">
            <a href="https://www.youtube.com/watch?v=_sI_Ps7JSEk" class="fancybox btn-play"><i class="icon-play"></i></a>
            <p class="latest-videos__item--text">Почему нефть будет <br>поддерживаться в долгосрочной <br> перспективе до $ 100</p>
          </div>
        </div>
      </div>
      <div class="col-4">
        <div class="latest-videos__item">
          <img src="data/latest-videos.png" alt="">
          <div class="latest-videos__item--overlay">
            <a href="https://www.youtube.com/watch?v=_sI_Ps7JSEk" class="fancybox btn-play"><i class="icon-play"></i></a>
            <p class="latest-videos__item--text">Почему цена на серебро <br> посылает предупреждение SPX</p>
          </div>
        </div>
      </div>
      <div class="col-4">
        <div class="latest-videos__item">
          <img src="https://via.placeholder.com/363x363" alt="">
          <div class="latest-videos__item--overlay">
            <a href="https://www.youtube.com/watch?v=_sI_Ps7JSEk" class="fancybox btn-play"><i class="icon-play"></i></a>
            <p class="latest-videos__item--text">Транспортный индекс Доу-Джонса <br> посылает предупреждения</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
