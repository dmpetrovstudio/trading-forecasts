<div class="services-block">
  <div class="container">
    <div class="row">
      <div class="col-4">
        <div class="services-block__item">
          <h3>Аналитика</h3>
          <p>Изучайте аналитику на основе волнового принципа Эллиотта</p>
        </div>
      </div>
      <div class="col-4">
        <div class="services-block__item">
          <h3>Обучение</h3>
          <p>Изучайте аналитику на основе волнового принципа Эллиотта</p>
        </div>
      </div>
      <div class="col-4">
        <div class="services-block__item">
          <h3>Инвестиции</h3>
          <p>Изучайте аналитику на основе волнового принципа Эллиотта</p>
        </div>
      </div>
    </div>
  </div>
</div>
