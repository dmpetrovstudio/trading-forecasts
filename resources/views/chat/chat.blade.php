@push('scripts')
<script src="{{ asset('js/chat.js') }}"></script>
@endpush

@extends('cabinet.cabinet')

@section('title', 'Чат')

@section('content')
<aside class="aside">
	<h4 class="messages-list__toggler">Сообщения</h4>
	<div class="messages-list scrollbar-inner">
		@foreach($list as $item)
		<a href="{{ url('chat/'.$item->id) }}" class="messages-list__item">
			@if($item->reciever->id != Auth::user()->id)
			<strong class="messages-list__item--header">{{ $item->reciever->name }}</strong>
			@else
			<strong class="messages-list__item--header">{{ $item->sender->name }}</strong>
			@endif
			@isset($item->message)
			<span class="messages-list__item--text">{{ $item->message->message }}</span>
			<span class="messages-list__item--time">{{ $item->message->created_at->format('d.m.Y H:i') }}</span>
			@endisset
		</a>
		@endforeach
	</div>
</aside>
<div class="content">
	<div id="chat" class="messages">{!! $dialog !!}</div>
</div>
@endsection
