@push('scripts')
<script src="{{ asset('js/chat.js') }}"></script>
@endpush

@extends('cabinet.cabinet')

@section('title', 'Подписчики')

@section('content')
<aside class="aside">
	<h4 class="messages-list__toggler">Подписчики</h4>
	<div class="messages-list scrollbar-inner">
		@foreach($list as $item)
		<a href="{{ url('chat/new/'.$item->subscriber->id) }}" class="messages-list__item">
			<strong class="messages-list__item--header">{{ $item->subscriber->name }}</strong>
			<span class="messages-list__item--time">{{ $item->created_at->format('d.m.Y H:i') }}</span>
		</a>
		@endforeach
	</div>
</aside>
<div class="content">
	<div id="chat" class="messages">{!! $dialog !!}</div>
</div>
@endsection
