<div class="messages-header online">
	<div class="messages-header__img">
		<a href="{{ url('profile/'.$reciever->name) }}">
			<img src="{{ url('images/avatar/icon/'.$reciever->avatar) }}" alt="" width="40px">
		</a>
	</div>
	<strong class="messages-header__name">
		<a href="{{ url('profile/'.$reciever->name) }}">{{ $reciever->name }}</a>
	</strong>
	@if($reciever->online)
	<span class="messages-header__status">online</span>
	@endif
</div>
<div id="chat" class="chat">
	<div class="scrollbar-inner">
		@php @$last_message_id = 0 @endphp
		@foreach($messages as $message)
		@php $last_message_id = $message->id @endphp
		<div class="chat-item @if($message->fk_reciever_id == $reciever->id) chat-item__admin @endif" data-id="{{ $message->id }}">
			<div class="chat-item__message">
				<div class="chat-item__header">
					<span class="chat-item__header--name">
						<a href="{{ url('profile/'.$message->reciever->name) }}">
							{{ $message->reciever->name }}
						</a>
					</span>
					<span class="chat-item__header--time">{{ $message->created_at->format('d.m.Y H:i') }}</span>
				</div>
				<p>{{ $message->message }}</p>
			</div>
		</div>
		@endforeach
	</div>
	<div class="chat-bottom">
		<form action="#" class="form chat-form">
			@csrf
			<input type="hidden" name="last_id" value="{{ $last_message_id }}" />
			<input type="hidden" name="dialog_id" value="{{ $dialog->id }}" />
			<input type="hidden" name="reciever" value="{{ $reciever->id }}" />
			<textarea name="message" placeholder="Введите текст сообщения"></textarea>
			<button class="btn">Отправить сообщение</button>
		</form>
	</div>
</div>
