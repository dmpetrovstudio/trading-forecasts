<div class="chat-item @if($message->fk_reciever_id == $reciever->id) chat-item__admin @endif" data-id="{{ $message->id }}">
	<div class="chat-item__message">
		<div class="chat-item__header">
			<span class="chat-item__header--name">
				<a href="{{ url('profile/'.$message->reciever->name) }}">
					{{ $message->reciever->name }}
				</a>
			</span>
			<span class="chat-item__header--time">{{ $message->created_at->format('d.m.Y H:i') }}</span>
		</div>
		<p>{{ $message->message }}</p>
	</div>
</div>
