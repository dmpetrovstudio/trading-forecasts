@extends('cabinet.cabinet')

@section('title', 'Мои публикации')

@section('content')
<aside class="aside hidden-xs">
	<h4>{{ $title }}</h4>
	<div class="latest-updates scrollbar-inner">
		@foreach($list as $item)
		<a href="{{ url('blog/item/'.$item->id) }}" class="latest-updates__item">
			<strong class="latest-updates__item--header">{{ $item->title }}</strong>
			<span class="latest-updates__item--name">{{ $item->user->name }}</span>
			<span class="latest-updates__item--time">{{ $item->created_at->format('d.m.Y H:i') }}</span>
		</a>
		@endforeach
	</div>
</aside>
<div class="content">

</div>
@endsection
