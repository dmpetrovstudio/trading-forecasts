@push('scripts')
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script src="{{ asset('js/dropzone.js') }}"></script>
<script src="{{ asset('js/blog.js') }}"></script>
<script type="text/javascript">

</script>
@endpush

@push('styles')
<link href="{{ asset('css/dropzone.css') }}" rel="stylesheet">
<link href="{{ asset('css/blog.css') }}" rel="stylesheet">
@endpush

@extends('cabinet.cabinet')

@section('title', 'Добавление публикации')

@section('content')
<aside class="aside">
	<h4>Что прикрепить</h4>
	<ul class="add-menu blog-tabs">
		<li><a href="#" data-tab="text" class="active">Текст</a></li>
		<li><a href="#" data-tab="photo">Главное изображение</a></li>
		<li><a href="#" data-tab="images">Изображение</a></li>
		<li><a href="#" data-tab="video">Видео</a></li>
		<li><a href="#" data-tab="voice">Голосовое сообщение</a></li>
	</ul>
</aside>
<div class="content">
	<div class="blog-form-content" data-tab="text">
		<form id="blogAddForm" action="{{ route('blog.add') }}" class="add-text" method="post" enctype="multipart/form-data">
			@csrf
			<input type="hidden" name="photo" />
			<input type="hidden" name="images" />
			<input type="hidden" name="video" />
			<input type="hidden" name="voice" />
			<input type="text" class="add-text__header" name="title" placeholder="Введите заголовок" value="{{ old('title') }}" required>
			<textarea id="add-text__text" class="add-text__text" name="text" placeholder="Введите текст…">{!! old('text') !!}</textarea>
			<br>
			<select name="trade" required class="form-field">
				<option>Выбор инструмента</option>
				@foreach($trades as $trade)
				<optgroup label="{{ $trade->name }}">
					@foreach($trade->list as $trade_item)
					<option value="{{ $trade_item->id }}">{{ $trade_item->name }}</option>
					@endforeach
				</optgroup>
				@endforeach
			</select>
			<select name="type" required class="form-field">
				<option>Выберете таймфрейм публикации</option>
				@foreach($types as $type)
				<option value="{{ $type->id }}">{{ $type->name }}</option>
				@endforeach
			</select>
			<select name="status" required class="form-field">
				<option>Статус идеи</option>
				@foreach($statuses as $status)
				<option value="{{ $status->id }}">{{ $status->name }}</option>
				@endforeach
			</select>
			<input type="submit" style="display: none;"/>
		</form>
	</div>
	<div class="blog-form-content" data-tab="photo" style="display: none;">
		<form id="dropzonePhoto" class="dropzone" action="{{ url('image/upload') }}" enctype="multipart/form-data" method="post">
			@csrf
			<input type="hidden" name="directory" value="blog" />
		</form>
	</div>
	<div class="blog-form-content" data-tab="images" style="display: none;">
		<form id="dropzoneImages" class="dropzone" action="{{ url('image/upload') }}" enctype="multipart/form-data" method="post">
			@csrf
			<input type="hidden" name="directory" value="blogitem" />
		</form>
	</div>
	<div class="blog-form-content" data-tab="video" style="display: none;">
		<form id="dropzoneVideo" class="dropzone" action="{{ url('image/upload') }}" enctype="multipart/form-data" method="post">
			@csrf
			<input type="hidden" name="directory" value="blogitem" />
		</form>
	</div>
	<div class="blog-form-content" data-tab="voice" style="display: none;">
		<form id="dropzoneVoice" class="dropzone" action="{{ url('image/upload') }}" enctype="multipart/form-data" method="post">
			@csrf
			<input type="hidden" name="directory" value="blogitem" />
		</form>
	</div>
	@if ($errors->any())
  <div class="alert alert-danger">
  	<ul>
    @foreach ($errors->all() as $error)
    	<li>{{ $error }}</li>
    @endforeach
    </ul>
  </div>
	@endif
	<button id="blogAddSubmit" type="submit" class="btn">Добавить</button>
</div>
@endsection
