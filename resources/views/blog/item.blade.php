@push('scripts')
<script src="{{ asset('js/comment.js') }}"></script>
@endpush

@push('styles')
<link href="{{ asset('css/blog.css') }}" rel="stylesheet">
@endpush

@extends('cabinet.cabinet')

@section('title', $post->title)

@section('content')
<aside class="aside hidden-xs">
	<h4>{{ $title }}</h4>
	<div class="latest-updates scrollbar-inner">
		@foreach($list as $item)
		<a href="{{ url('blog/item/'.$item->id) }}" class="latest-updates__item">
			<strong class="latest-updates__item--header">{{ $item->title }}</strong>
			<span class="latest-updates__item--name">{{ $item->user->name }}</span>
			<span class="latest-updates__item--time">{{ $item->created_at->format('d.m.Y H:i') }}</span>
		</a>
		@endforeach
	</div>
</aside>
<div class="content">
	<article class="article">
		<a href="{{ url('blog/my') }}" class="btn-back">Назад</a>
		<h3>{{ $post->title }}</h3>
		<div class="article-top justify-align">
			<span class="article-top__author">
				<a href="{{ url('profile/'.$post->user->name) }}">
					{{ $post->user->name }}
				</a>
			</span>
			<span class="article-top__time">{{ $post->created_at->format('d.m.Y H:i') }}</span>
		</div>
		@if($post->photo)
		<img src="{{ url('images/blog/post/'.$post->photo) }}" alt="">
		@endif
		<div class="row">
			<div class="col-6">
				<div class="article-chart">
					@php $images = explode(",", $post->images); @endphp
					@foreach($images as $image)
					<img src="{{ url('images/blogitem/post/'.$image) }}" alt="">
					@endforeach
				</div>
			</div>
			<div class="col-6">
				<div class="article-info">
					{!! $post->text !!}
					@php $voices = explode(",", $post->voice); @endphp
					@foreach($voices as $voice)
					<div class="voice">
						<audio id=audio src="{{ Storage::disk('blogitem')->url($voice) }}" controls></audio>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</article>
	{!! $comments_list !!}
</div>
@endsection
