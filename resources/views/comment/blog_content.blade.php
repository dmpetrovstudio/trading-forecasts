<div class="comments">
	<h3>Все комментарии <span>{{ $count }}</span></h3>
	<form class="comments-form">
		@csrf
		<input type="hidden" name="blog_id" value="{{ $post_id }}" />
		<input type="hidden" name="comment_id" value="" />
		<textarea name="message" placeholder="Оставьте ваш комментарий"></textarea>
		<button class="btn">Оставить комментарий</button>
	</form>
	<div class="comments-block">
		{!! $comments_list !!}
	</div>
</div>
