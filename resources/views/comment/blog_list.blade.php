@foreach($comments as $comment)
<div class="comments-item">
	<a href="{{ url('profile/'.$comment->author->name) }}">
		<img src="{{ url('images/avatar/icon/'.$comment->author->avatar) }}" alt="">
	</a>
	<div class="comments-item__post">
		<div class="comments-item__author">
			<a href="{{ url('profile/'.$comment->author->name) }}">
				{{ $comment->author->name }}
			</a>
		</div>
		<p>{{ $comment->message }}</p>
		<div class="comments-item__bottom">
			<span class="comments-item__time">{{ $comment->created_at->format('d.m.Y H:i') }}</span>
			<a href="javascript:void(0)" class="comments-item__reply" data-id="{{ $comment->id }}" data-author="{{ $comment->author->name }}">Ответить</a>
		</div>
	</div>
</div>
@foreach($comment->list as $level2)
<div class="comments-item level-2">
	<a href="{{ url('profile/'.$level2->author->name) }}">
		<img src="{{ url('images/avatar/icon/'.$level2->author->avatar) }}" alt="">
	</a>
	<div class="comments-item__post">
		<div class="comments-item__author">
			<a href="{{ url('profile/'.$level2->author->name) }}">
				{{ $level2->author->name }}
			</a>
		</div>
		<p><a href="javascript:void(0);">{{ '@'.$comment->author->name }},</a> {{ $level2->message }}</p>
		<div class="comments-item__bottom">
			<span class="comments-item__time">{{ $level2->created_at->format('d.m.Y H:i') }}</span>
			<a href="javascript:void(0)" class="comments-item__reply" data-id="{{ $comment->id }}" data-author="{{ $level2->author->name }}">Ответить</a>
		</div>
	</div>
</div>
@endforeach
@endforeach
