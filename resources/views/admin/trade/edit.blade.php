@extends('adminlte::page')

@section('title', 'Редактирование рынка')

@section('content_header')
    <h1>Редактирование рынка</h1>
@stop

@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">{{ $item->name }}</h3>
  </div>
  <form class="form" action="{{ route('admin.trade.editform') }}" method="post">
    @csrf
    <input type="hidden" name="id" value="{{ $item->id }}" />
    <div class="box-body">
      <div class="form-group">
        <label for="inputName">Название</label>
        <input type="text" name="name" class="form-control" id="inputName" placeholder="Название" value="{{ $item->name }}">
      </div>
    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Сохранить</button>
    </div>
  </form>
@stop
