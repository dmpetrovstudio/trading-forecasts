@extends('adminlte::page')

@section('title', 'Рынок')

@section('content_header')
    <h1>Рынок</h1>
@stop

@section('content')
  <form>
    <table class="table">
      <thead>
        <tr class="info">
          <th style="width:5%;">№</th>
          <th style="width:75%;">Название</th>
          <th style="width:5%;"></th>
          <th style="width:5%;"></th>
          <th style="width:5%;"></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td></td>
          <td colspan="4">
            <a href="{{ url('admin/trade/add') }}" class="btn btn-info"><i class="fa fa-plus"></i> Добавить</a>
          </td>
        </tr>
        @foreach($list as $item)
        <tr>
          <td>{{ $item->id }}</td>
          <td>{{ $item->name }}</td>
          <td>
            <a class="btn btn-info" data-toggle="tooltip" title="Добавить внутрь" href="{{ url('admin/trade/add/'.$item->id) }}"><i class="fa fa-plus"></i></a>
          </td>
          <td>
            <a class="btn btn-success" data-toggle="tooltip" title="Редактировать" href="{{ url('admin/trade/edit/'.$item->id) }}"><i class="fa fa-pencil-alt"></i></a>
          </td>
          <td>
            <a class="btn btn-danger" data-toggle="tooltip" title="Удалить" href="{{ url('admin/trade/trash/'.$item->id) }}"><i class="fa fa-trash"></i></a>
          </td>
        </tr>
        @foreach($item->list as $row)
        <tr>
          <td></td>
          <td colspan="2">{{ $row->name }}</td>
          <td>
            <a class="btn btn-success" data-toggle="tooltip" title="Редактировать" href="{{ url('admin/trade/edit-item/'.$row->id) }}"><i class="fa fa-pencil-alt"></i></a>
          </td>
          <td>
            <a class="btn btn-danger" data-toggle="tooltip" title="Удалить" href="{{ url('admin/trade/trash-item/'.$row->id) }}"><i class="fa fa-trash"></i></a>
          </td>
        </tr>
        @endforeach
        @endforeach
      </tbody>
    </table>
  </form>
@stop
