@extends('adminlte::page')

@section('title', 'Добавление рынка')

@section('content_header')
    <h1>Добавление рынка</h1>
@stop

@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Новый рынок</h3>
  </div>
  <form class="form" action="{{ route('admin.trade.addform') }}" method="post">
    @csrf
    <input type="hidden" name="parent" value="{{ $id }}" />
    <div class="box-body">
      <div class="form-group">
        <label for="inputName">Название</label>
        <input type="text" name="name" class="form-control" id="inputName" placeholder="Название">
      </div>
    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Добавить</button>
    </div>
  </form>
@stop
