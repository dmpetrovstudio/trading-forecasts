@section('js')
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script src="{{ asset('js/dropzone.js') }}"></script>
<script type="text/javascript">
  CKEDITOR.replace('inputText', {
    toolbar: [
            { name: 'document', items: [ 'NewPage', 'Preview', '-', 'Templates' ] },
            { name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
            { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Strike' ] }
        ]
  });
  Dropzone.options.dropzonePhoto =
	{
		maxFilesize: 3,
		renameFile: function(file) {
				var dt = new Date();
				var time = dt.getTime();
			 return time+file.name;
		},
		acceptedFiles: ".jpeg,.jpg,.png,.gif",
		addRemoveLinks: true,
		timeout: 5000,
		dictDefaultMessage: '<span class="add-image__icon dz-message"><i class="icon-image"></i><strong class="hidden-xs">Нажмите или перетащите чтобы добавить изображение</strong><strong class="visible-xs">Загрузить изображение</strong></span>',
		init: function(){
       //$(this.element).html($(this.element).html() + this.options.dictDefaultMessage);
    },
    processing: function()
    {
    	$(this.element).find('.dz-message').remove();
    },
		success: function(file, response)
		{
			console.log(response);
			if(response.success)
			{
				var _photo = $('#blogAddForm [name=photo]').val();
				if(_photo.length > 0)
					$('#blogAddForm [name=photo]').val(_photo + ',' + response.success);
				else
					$('#blogAddForm [name=photo]').val(response.success);
			}
		},
		error: function(file, response)
		{
       console.log(response);
       alert(response.message);
			 return false;
		},
		removedfile: function(file)
		{
				var name = file.upload.filename;
				$.ajax({
						headers: {
												'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
										},
						type: 'POST',
						url: '{{ url("image/delete") }}',
						data: {filename: name},
						success: function (data){
								console.log("File has been successfully removed!!");
						},
						error: function(e) {
								console.log(e);
						}});
						var fileRef;
						return (fileRef = file.previewElement) != null ?
						fileRef.parentNode.removeChild(file.previewElement) : void 0;
		},
	};
	Dropzone.options.dropzoneImages =
	{
		maxFilesize: 3,
		renameFile: function(file) {
				var dt = new Date();
				var time = dt.getTime();
			 return time+file.name;
		},
		acceptedFiles: ".jpeg,.jpg,.png,.gif",
		addRemoveLinks: true,
		timeout: 5000,
		dictDefaultMessage: '<span class="add-image__icon dz-message"><i class="icon-image"></i><strong class="hidden-xs">Нажмите или перетащите чтобы добавить изображение</strong><strong class="visible-xs">Загрузить изображение</strong></span>',
		init: function(){
       //$(this.element).html(this.options.dictDefaultMessage);
    },
    processing: function()
    {
    	$(this.element).find('.dz-message').remove();
    },
		success: function(file, response)
		{
			console.log(response);
			if(response.success)
			{
				var _photo = $('#blogAddForm [name=images]').val();
				if(_photo.length > 0)
					$('#blogAddForm [name=images]').val(_photo + ',' + response.success);
				else
					$('#blogAddForm [name=images]').val(response.success);
			}
		},
		error: function(file, response)
		{
       console.log(response);
       alert(response.message);
			 return false;
		},
		removedfile: function(file)
		{
				var name = file.upload.filename;
				$.ajax({
						headers: {
												'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
										},
						type: 'POST',
						url: '{{ url("image/delete") }}',
						data: {filename: name},
						success: function (data){
								console.log("File has been successfully removed!!");
						},
						error: function(e) {
								console.log(e);
						}});
						var fileRef;
						return (fileRef = file.previewElement) != null ?
						fileRef.parentNode.removeChild(file.previewElement) : void 0;
		},
	};
	Dropzone.options.dropzoneVideo =
	{
		maxFilesize: 50,
		renameFile: function(file) {
				var dt = new Date();
				var time = dt.getTime();
			 return time+file.name;
		},
		acceptedFiles: ".mp4",
		addRemoveLinks: true,
		timeout: 5000,
		dictDefaultMessage: '<span class="add-image__icon dz-message"><i class="icon-image"></i><strong class="hidden-xs">Нажмите или перетащите чтобы добавить видеофайл</strong><strong class="visible-xs">Загрузить видеофайл</strong></span>',
		init: function(){
       //$(this.element).html(this.options.dictDefaultMessage);
    },
    processing: function()
    {
    	$(this.element).find('.dz-message').remove();
    },
		success: function(file, response)
		{
			console.log(response);
			if(response.success)
			{
				var _photo = $('#blogAddForm [name=video]').val();
				if(_photo.length > 0)
					$('#blogAddForm [name=video]').val(_photo + ',' + response.success);
				else
					$('#blogAddForm [name=video]').val(response.success);
			}
		},
		error: function(file, response)
		{
      console.log(response);
      alert(response.message);
			return false;
		},
		removedfile: function(file)
		{
				var name = file.upload.filename;
				$.ajax({
						headers: {
												'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
										},
						type: 'POST',
						url: '{{ url("image/delete") }}',
						data: {filename: name},
						success: function (data){
								console.log("File has been successfully removed!!");
						},
						error: function(e) {
								console.log(e);
						}});
						var fileRef;
						return (fileRef = file.previewElement) != null ?
						fileRef.parentNode.removeChild(file.previewElement) : void 0;
		},
	};
	Dropzone.options.dropzoneVoice =
	{
		maxFilesize: 10,
		renameFile: function(file) {
				var dt = new Date();
				var time = dt.getTime();
			 return time+file.name;
		},
		acceptedFiles: ".mp3",
		addRemoveLinks: true,
		timeout: 5000,
		dictDefaultMessage: '<span class="add-image__icon dz-message"><i class="icon-image"></i><strong class="hidden-xs">Нажмите или перетащите чтобы добавить аудиофайл</strong><strong class="visible-xs">Загрузить аудиофайл</strong></span>',
		init: function(){
       //$(this.element).html(this.options.dictDefaultMessage);
    },
    processing: function()
    {
    	$(this.element).find('.dz-message').remove();
    },
		success: function(file, response)
		{
			console.log(response);
			if(response.success)
			{
				var _photo = $('#blogAddForm [name=voice]').val();
				if(_photo.length > 0)
					$('#blogAddForm [name=voice]').val(_photo + ',' + response.success);
				else
					$('#blogAddForm [name=voice]').val(response.success);
			}
		},
		error: function(file, response)
		{
      console.log(response);
      alert(response.message);
			return false;
		},
		removedfile: function(file)
		{
				var name = file.upload.filename;
				$.ajax({
						headers: {
												'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
										},
						type: 'POST',
						url: '{{ url("image/delete") }}',
						data: {filename: name},
						success: function (data){
								console.log("File has been successfully removed!!");
						},
						error: function(e) {
								console.log(e);
						}});
						var fileRef;
						return (fileRef = file.previewElement) != null ?
						fileRef.parentNode.removeChild(file.previewElement) : void 0;
		},
	};
</script>
@stop

@section('css')
<link href="{{ asset('css/dropzone.css') }}" rel="stylesheet">
@stop

@extends('adminlte::page')

@section('title', 'Редактирование публикации')

@section('content_header')
    <h1>Редактирование публикации</h1>
@stop

@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">{{ $item->title }}</h3>
  </div>
  <form id="blogAddForm" class="form" action="{{ route('admin.blog.editform') }}" method="post">
    @csrf
    <input type="hidden" name="id" value="{{ $item->id }}" />
    <input type="hidden" name="photo" value="{{ $item->photo }}" />
    <input type="hidden" name="images" value="{{ $item->images }}" />
    <input type="hidden" name="video" value="{{ $item->video }}" />
    <input type="hidden" name="voice" value="{{ $item->voice }}" />
    <div class="box-body">
      <div class="form-group">
        <label for="inputTitle">Заголовок</label>
        <input type="text" name="title" class="form-control" id="inputTitle" placeholder="Заголовок" value="{{ $item->title }}">
      </div>
      <div class="form-group">
        <label for="inputText">Текст</label>
        <textarea name="text" class="form-control" id="inputText" placeholder="Текст">{{ $item->text }}</textarea>
      </div>
      <div class="form-group">
        <label for="inputTitle">Тип</label>
        <select class="form-control" name="type">
          <option></option>
          @foreach($types as $type)
          <option value="{{ $type->id }}"@php if($type->id == $item->fk_blog_type_id) echo " selected='selected'" @endphp>{{ $type->name }}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="inputTitle">Рынок</label>
        <select class="form-control" name="trade">
          <option></option>
          @foreach($trades as $trade)
          <optgroup label="{{ $trade->name }}">
            @foreach($trade->list as $trade_item)
            <option value="{{ $trade_item->id }}"@php if($trade_item->id == $item->fk_trade_id) echo " selected='selected'" @endphp>{{ $trade_item->name }}</option>
            @endforeach
          </optgroup>
          @endforeach
        </select>
      </div>
    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Сохранить</button>
    </div>
  </form>
  <div class="box-body">
    <div class="form-group">
      <label for="inputTitle">Главное изображение</label>
      <form id="dropzonePhoto" class="dropzone" action="{{ url('image/upload') }}" enctype="multipart/form-data" method="post">
        @csrf
        <input type="hidden" name="directory" value="blog" />
        @php $photo = explode(",", $item->photo) @endphp
        @foreach($photo as $photo_item)
        <div class="dz-preview dz-image-preview dz-complete">
          <div class="dz-image">
            <img data-dz-thumbnail="" alt="{{ $photo_item }}" src="{{ url('images/blog/icon/'.$photo_item) }}">
          </div>
          <div class="dz-details">
            <div class="dz-filename">
              <span data-dz-name="">{{ $photo_item }}</span>
            </div>
          </div>
          <div class="dz-success-mark"></div>
          <a class="dz-remove" href="javascript:undefined;" data-dz-remove="">Remove file</a>
        </div>
        @endforeach
      </form>
    </div>
    <div class="form-group">
      <label for="inputTitle">Изображения</label>
      <form id="dropzoneImages" class="dropzone" action="{{ url('image/upload') }}" enctype="multipart/form-data" method="post">
        @csrf
        <input type="hidden" name="directory" value="blogitem" />
        @php $photo = explode(",", $item->images) @endphp
        @foreach($photo as $photo_item)
        <div class="dz-preview dz-image-preview dz-complete">
          <div class="dz-image">
            <img data-dz-thumbnail="" alt="{{ $photo_item }}" src="{{ url('images/blogitem/icon/'.$photo_item) }}">
          </div>
          <div class="dz-details">
            <div class="dz-filename">
              <span data-dz-name="">{{ $photo_item }}</span>
            </div>
          </div>
          <div class="dz-success-mark"></div>
          <a class="dz-remove" href="javascript:undefined;" data-dz-remove="">Remove file</a>
        </div>
        @endforeach
      </form>
    </div>
    <div class="form-group">
      <label for="inputTitle">Видео</label>
      <form id="dropzoneVideo" class="dropzone" action="{{ url('image/upload') }}" enctype="multipart/form-data" method="post">
        @csrf
        <input type="hidden" name="directory" value="blogitem" />
        @php $photo = explode(",", $item->video) @endphp
        @foreach($photo as $photo_item)
        <div class="dz-preview dz-image-preview dz-complete">
          <div class="dz-image">
            <img data-dz-thumbnail="" alt="{{ $photo_item }}" src="{{ Storage::disk('blogitem')->url($photo_item) }}">
          </div>
          <div class="dz-details">
            <div class="dz-filename">
              <span data-dz-name="">{{ $photo_item }}</span>
            </div>
          </div>
          <div class="dz-success-mark"></div>
          <a class="dz-remove" href="javascript:undefined;" data-dz-remove="">Remove file</a>
        </div>
        @endforeach
      </form>
    </div>
    <div class="form-group">
      <label for="inputTitle">Голосовое сообщение</label>
      <form id="dropzoneVoice" class="dropzone" action="{{ url('image/upload') }}" enctype="multipart/form-data" method="post">
        @csrf
        <input type="hidden" name="directory" value="blogitem" />
        @php $photo = explode(",", $item->voice) @endphp
        @foreach($photo as $photo_item)
        <div class="dz-preview dz-image-preview dz-complete">
          <div class="dz-image">
            <img data-dz-thumbnail="" alt="{{ $photo_item }}" src="{{ Storage::disk('blogitem')->url($photo_item) }}">
          </div>
          <div class="dz-details">
            <div class="dz-filename">
              <span data-dz-name="">{{ $photo_item }}</span>
            </div>
          </div>
          <div class="dz-success-mark"></div>
          <a class="dz-remove" href="javascript:undefined;" data-dz-remove="">Remove file</a>
        </div>
        @endforeach
      </form>
    </div>
  </div>
@stop
