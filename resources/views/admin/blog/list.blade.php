@extends('adminlte::page')

@section('title', 'Публикации')

@section('content_header')
    <h1>Публикации</h1>
@stop

@section('content')
  <form>
    <table class="table">
      <thead>
        <tr class="info">
          <th style="width:5%;">№</th>
          <th style="width:35%;">Название</th>
          <th style="width:10%;">Тип</th>
          <th style="width:10%;">Рынок</th>
          <th style="width:10%;">Автор</th>
          <th style="width:15%;">Дата добавления</th>
          <th style="width:5%;"></th>
          <th style="width:5%;"></th>
          <th style="width:5%;"></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td></td>
          <td colspan="7">
            <a href="{{ url('admin/blog/add') }}" class="btn btn-info"><i class="fa fa-plus"></i> Добавить</a>
          </td>
        </tr>
        <tr>
          <td></td>
          <td>
            <input type="text" class="form-control" name="title" placeholder="Введите название статьи" value="{{ app()->request->title }}"/>
          </td>
          <td>
            <select class="form-control" name="type">
              <option></option>
              @foreach($types as $type)
              <option value="{{ $type->id }}"@php if($type->id == app()->request->type) echo " selected='selected'" @endphp>{{ $type->name }}</option>
              @endforeach
            </select>
          </td>
          <td>
            <select class="form-control" name="trade">
              <option></option>
              @foreach($trades as $trade)
              <optgroup label="{{ $trade->name }}">
                @foreach($trade->list as $trade_item)
                <option value="{{ $trade_item->id }}"@php if($trade_item->id == app()->request->trade) echo " selected='selected'" @endphp>{{ $trade_item->name }}</option>
                @endforeach
              </optgroup>
              @endforeach
            </select>
          </td>
          <td>
            <select class="form-control" name="author">
              <option></option>
              @foreach($authors as $author)
              <option value="{{ $author->id }}"@php if($author->id == app()->request->author) echo " selected='selected'" @endphp>{{ $author->name }}</option>
              @endforeach
            </select>
          </td>
          <td>
            <button type="submit" class="btn btn-success">Поиск</button>
          </td>
          <td colspan="3"></td>
        </tr>
        @foreach($list as $item)
        <tr@php if($item->status == 2) echo " class='warning'" @endphp>
          <td>{{ $item->id }}</td>
          <td>{{ $item->title }}</td>
          <td>{{ $item->blogtype->name }}</td>
          <td>{{ $item->tradeitem->name }}</td>
          <td>{{ $item->user->name }}</td>
          <td>{{ $item->created_at->format('d.m.Y H:i:s') }}</td>
          <td>
            <a class="btn btn-@php if($item->status == 2) echo "danger"; else echo "warning"; @endphp" title="@php if($item->status == 2) echo "Показать"; else echo "Скрыть"; @endphp" href="{{ url('admin/blog/hide/'.$item->id) }}"><i class="fa fa-eye"></i></a>
          </td>
          <td>
            <a class="btn btn-success" title="Редактировать" href="{{ url('admin/blog/edit/'.$item->id) }}"><i class="fa fa-pencil-alt"></i></a>
          </td>
          <td>
            <a class="btn btn-danger" title="Удалить" href="{{ url('admin/blog/trash/'.$item->id) }}"><i class="fa fa-trash"></i></a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </form>
@stop
