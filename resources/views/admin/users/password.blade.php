@extends('adminlte::page')

@section('title', 'Смена пароля')

@section('content_header')
    <h1>Смена пароля</h1>
@stop

@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Смена пароля для пользователя {{ $item->name }}</h3>
  </div>
  <form class="form" action="{{ route('admin.users.password') }}" method="post">
    @csrf
    <input type="hidden" name="id" value="{{ $item->id }}" />
    <div class="box-body">
      <div class="form-group">
        <label for="inputPassword">Новый пароль</label>
        <input type="password" name="password" class="form-control" id="inputPassword" placeholder="Введите новый пароль" />
      </div>
      <div class="form-group">
        <label for="inputPassword2">Повторите новый пароль</label>
        <input type="password" name="password2" class="form-control" id="inputPassword2" placeholder="Введите новый пароль ещё раз" />
      </div>
    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Сохранить</button>
    </div>
  </form>
@stop
