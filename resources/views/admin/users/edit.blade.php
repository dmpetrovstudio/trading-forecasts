@extends('adminlte::page')

@section('title', 'Редактирование пользователя')

@section('content_header')
    <h1>Редактирование пользователя</h1>
@stop

@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">{{ $item->name }}</h3>
  </div>
  <form class="form" action="{{ route('admin.users.editform') }}" method="post">
    @csrf
    <input type="hidden" name="id" value="{{ $item->id }}" />
    <div class="box-body">
      <div class="form-group">
        <label for="inputName">Имя</label>
        <input type="text" name="name" class="form-control" id="inputName" placeholder="Имя" value="{{ $item->name }}">
      </div>
      <div class="form-group">
        <label for="inputEmail">Email</label>
        <input type="text" name="email" class="form-control" id="inputEmail" placeholder="Email" value="{{ $item->email }}">
      </div>
      <div class="form-group">
        <div class="col-sm-12">
          <div class="checkbox">
            <label>
              <input type="checkbox" name="main_author"@php if($item->main_author) echo " checked='cheked'" @endphp> Отображать на странице регистрации
            </label>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label for="inputTitle">Роль</label>
        <select class="form-control" name="role">
          <option></option>
          @foreach($roles as $role)
          <option value="{{ $role->id }}"@php if($role->id == $item->fk_role_id) echo " selected='selected'" @endphp>{{ $role->name }}</option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Сохранить</button>
    </div>
  </form>
@stop
