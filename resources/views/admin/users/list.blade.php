@extends('adminlte::page')

@section('title', 'Пользователи')

@section('content_header')
    <h1>Пользователи</h1>
@stop

@section('content')
  <form>
    <table class="table">
      <thead>
        <tr class="info">
          <th>№</th>
          <th>Логин</th>
          <th>Email</th>
          <th>Дата регистрации</th>
          <th>Реферал</th>
          <th>Роль</th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
        <tr class="info">
          <th></th>
          <th>
            <input type="text" class="form-control" name="name" value="{{ app()->request->name }}" />
          </th>
          <th>
            <input type="text" class="form-control" name="email" value="{{ app()->request->email }}" />
          </th>
          <th></th>
          <th></th>
          <th>
            <select class="form-control" name="role">
              <option></option>
              @foreach($roles as $role)
              <option value="{{ $role->id }}"@php if($role->id == app()->request->role) echo " selected='selected'"; @endphp>{{ $role->name }}</option>
              @endforeach
            </select>
          </th>
          <th colspan="4">
            <button type="submit" class="btn btn-success">Поиск</button>
          </th>
        </tr>
      </thead>
      <tbody>
        @foreach($list as $item)
        <tr>
          <td>{{ $item->id }}</td>
          <td>{{ $item->name }}</td>
          <td>{{ $item->email }}</td>
          <td>{{ $item->created_at->format('d.m.Y H:i:s') }}</td>
          <td>@php if(isset($item->referalUser)) echo $item->referalUser->name @endphp</td>
          <td>{{ $item->role->name }}</td>
          <td>
            <a class="btn btn-warning" href="{{ url('admin/users/hide/'.$item->id) }}"><i class="fa fa-eye"></i></a>
          </td>
          <td>
            <a class="btn btn-info" href="{{ url('admin/users/password/'.$item->id) }}"><i class="fa fa-key"></i></a>
          </td>
          <td>
            <a class="btn btn-success" href="{{ url('admin/users/edit/'.$item->id) }}"><i class="fa fa-pencil-alt"></i></a>
          </td>
          <td>
            <a class="btn btn-danger" href="{{ url('admin/users/trash/'.$item->id) }}"><i class="fa fa-trash"></i></a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{ $list->links() }}
  </form>
@stop
