@section('title', 'Главная')
@extends('layouts.app')
  @include('main.header')
	<div class="content">
    @include('main.banner')
    @include('main.video')
    @include('main.services')
	</div>
	<!-- END of .content -->
