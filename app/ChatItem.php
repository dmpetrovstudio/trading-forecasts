<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatItem extends Model
{
    /**
     * Таблица, связанная с моделью.
     *
     * @var string
     */
    protected $table = 'chat';

    public function reciever()
    {
        return $this->hasOne('\App\User', 'id', 'fk_reciever_id');
    }

    public function sender()
    {
        return $this->hasOne('\App\User', 'id', 'fk_sender_id');
    }
}
