<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * Таблица, связанная с моделью.
     *
     * @var string
     */
    protected $table = 'comment';

    public function author()
    {
        return $this->hasOne('\App\User', 'id', 'fk_user_id');
    }

    public function list()
    {
      return $this->hasMany('App\Comment', 'fk_comment_id');
      //  return $this->belongsToMany(Comment::class, 'comment', 'fk_comment_id', 'id');
    }
}
