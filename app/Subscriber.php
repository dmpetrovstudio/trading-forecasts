<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    /**
     * Таблица, связанная с моделью.
     *
     * @var string
     */
    protected $table = 'users_subscribers';

    protected $fillable = ['fk_user_id', 'fk_trade_id', 'fk_subscriber_id'];

    public function list()
    {
      return $this->belongsToMany(User::class, 'users_subscribers', 'id', 'fk_user_id');
    }

    public function subscriber()
    {
        return $this->hasOne('\App\User', 'id', 'fk_subscriber_id');
    }
}
