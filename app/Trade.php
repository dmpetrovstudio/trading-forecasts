<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trade extends Model
{
    /**
     * Таблица, связанная с моделью.
     *
     * @var string
     */
    protected $table = 'trade';

    public $timestamps  = false;

    public function list()
    {
        return $this->hasMany('\App\TradeItem', 'fk_trade_id', 'id')->where('status', 1);
    }
}
