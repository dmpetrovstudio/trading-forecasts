<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTrade extends Model
{
    /**
     * Таблица, связанная с моделью.
     *
     * @var string
     */
    protected $table = 'users_trade';

    public $timestamps  = false;

    protected $fillable = [
        'price', 'fk_user_id', 'fk_trade_item_id'
    ];
}
