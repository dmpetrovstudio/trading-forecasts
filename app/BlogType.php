<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogType extends Model
{
    /**
     * Таблица, связанная с моделью.
     *
     * @var string
     */
    protected $table = 'blog_type';

    public $timestamps = false;
}
