<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\UserTrade;

class TradeItem extends Model
{
    /**
     * Таблица, связанная с моделью.
     *
     * @var string
     */
    protected $table = 'trade_item';

    public $timestamps = false;

    public function trade()
    {
        return $this->hasOne('\App\Trade', 'id', 'fk_trade_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'users_trade', 'fk_trade_item_id', 'fk_user_id');
    }

    public function userPrice()
    {
      return $this->hasOne(UserTrade::class, 'id', 'fk_trade_id');
    }

    public function usersTradePrice()
    {
      $item = UserTrade::where('fk_user_id', Auth::user()->id)->where('fk_trade_item_id', $this->id)->first();
      if(is_object($item))
        return $item->price;
      return 0;
    }

    public function usersTradeFrequency()
    {
      $item = UserTrade::where('fk_user_id', Auth::user()->id)->where('fk_trade_item_id', $this->id)->first();
      if(is_object($item))
        return $item->frequency;
      return 0;
    }
}
