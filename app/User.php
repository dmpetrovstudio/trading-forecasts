<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MailResetPasswordNotification;
use Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'name', 'email', 'password', 'referal', 'avatar', 'fk_referal_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $count_user = null;
    protected $count_comment = null;
    protected $count_blog = null;

    public function isAdmin()
    {
        if($this->fk_role_id == 1)
            return true;
        return false;
    }

    public function role()
    {
        return $this->hasOne('\App\Role', 'id', 'fk_role_id');
    }

    public function referalUser()
    {
        return $this->hasOne('\App\User', 'id', 'fk_referal_id');
    }

    public function trades()
    {
        return $this->belongsToMany(TradeItem::class, 'users_trade', 'fk_user_id', 'fk_trade_item_id');
    }

    public function getSubscriberTrades()
    {
        return TradeItem::select('trade_item.*')->
                      leftJoin('users_subscribers', 'users_subscribers.fk_trade_id', 'trade_item.id')->
                      where('users_subscribers.fk_subscriber_id', Auth::user()->id)->
                      where('users_subscribers.fk_user_id', $this->fk_user_id)->
                      get();
    }

    public function countUsers()
    {
      if(is_null($this->count_user))
        $this->count_user = \DB::table('users_subscribers')->where('fk_user_id', $this->id)->groupBy('fk_subscriber_id')->get()->count();
      return $this->count_user;
    }

    public function countComment()
    {
      if(is_null($this->count_comment))
        $this->count_comment = \DB::table('comment')->where('fk_user_id', $this->id)->get()->count();
      return $this->count_comment;
    }

    public function countBlog()
    {
      if(is_null($this->count_blog))
        $this->count_blog = \DB::table('blog_item')->where('fk_user_id', $this->id)->get()->count();
      return $this->count_blog;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordNotification($token));
    }
}
