<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatDialog extends Model
{
    /**
     * Таблица, связанная с моделью.
     *
     * @var string
     */
    protected $table = 'chat_dialog';

    public function reciever()
    {
        return $this->hasOne('\App\User', 'id', 'fk_reciever_id');
    }

    public function sender()
    {
        return $this->hasOne('\App\User', 'id', 'fk_creator_id');
    }
}
