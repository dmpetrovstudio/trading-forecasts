<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\UploadTrait;
use Image;
use Auth;
use App\Trade;
use App\BlogType;
use App\BlogItem;
use App\BlogStatus;

class BlogController extends Controller
{
    use UploadTrait;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function add()
    {
      $trades = Trade::where('status', 1)->get();
      $types = BlogType::where('status', 1)->get();
      $statuses = BlogStatus::where('status', 1)->get();

      return view('blog.add', [ 'trades' => $trades, 'types' => $types, 'statuses' => $statuses ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function my()
    {
      $list = BlogItem::where('status', 1)->where('fk_user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();
      return view('blog.my', [ "list" => $list, "title" => "Мои публикации" ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function last()
    {
      $list = BlogItem::where('status', 1)->where('created_at', '>', date("Y-m-d H:i:s", time() - 7*86400))->orderBy('created_at', 'desc')->get();
      return view('blog.my', [ "list" => $list, "title" => "Последние обновления"]);
    }

    public function item($id)
    {
      if(Auth::user()->fk_role_id == 2)
        $list = BlogItem::where('status', 1)->where('fk_user_id', \Auth::user()->id)->orderBy('created_at', 'desc')->get();
      else
        $list = BlogItem::where('status', 1)->orderBy('created_at', 'desc')->get();
      $post = BlogItem::where('status', 1)->where('id', $id)->firstOrFail();
      $title = Auth::user()->fk_role_id == 2 ? "Мои публикации" : "Последние обновления";

      $comments = app('App\Http\Controllers\CommentController')->showBlogList($post->id);

      return view('blog.item', [ "list" => $list, "post" => $post, "title" => $title, "comments_list" => $comments ]);
    }

    /**
     * Save profile information
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
      $messages = array(
        'required' => 'Поле :attribute должно быть заполнено.',
      );

      $validator = \Validator::make($request->all(), [
            'title'             =>  'required',
            'text'              =>  'required',
        ], $messages);
      if($validator->fails()) {
        return redirect('blog/add')
                    ->withErrors($validator)
                    ->withInput();
      }

      $item = new BlogItem;
      $item->title = $request->title;
      $item->text = $request->text;
      $item->fk_trade_id = $request->trade;
      $item->fk_blog_type_id = $request->type;
      $item->photo = $request->photo;
      $item->images = $request->images;
      $item->video = $request->video;
      $item->voice = $request->voice;
      $item->status = $request->status;
      $item->fk_user_id = Auth::user()->id;
      $item->save();
      return \Redirect::to("blog/my");
    }

    /**
     * Save profile information
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function save(Request $request)
    {
      $user = \App\User::find(Auth::user()->id);
      $user->name = $request->name;
      $user->email = $request->email;
      $user->save();
      return \Redirect::to("blog/my");
    }

    public function authorList($author)
    {
      $list = BlogItem::leftJoin('users', 'users.id', '=', 'blog_item.fk_user_id')->where('users.name', $author)->where('blog_item.status', 1)->orderBy('blog_item.created_at', 'desc')->get();

      return view('blog.my', [ "list" => $list, "title" => "Публикации $author"]);
    }
}
