<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\ImageUpload;
use App\ImageFile;

class ImageUploadController extends Controller
{
  private $size = Array("avatar" => Array("width" => 160, "height" => 160),
                        "blog" => Array("width" => 993, "height" => 344),
                        "blogitem" => Array("width" => 561, "height" => 288));

  public function fileCreate(Request $request)
  {
    return $this->fileStore($request);
    return view('imageupload');
  }

  public function fileStore(Request $request)
  {
    $image = $request->file('file');
    $extension = $image->getClientOriginalExtension();
    $directory = $request->directory;
    $imageName = $image->getClientOriginalName();
    Storage::disk($directory)->putFileAs('', $image, $imageName);
    if(in_array(strtolower($extension), Array("jpg", "png", "gif")))
    {
      //Storage::disk($directory)->putFileAs('thumbnail', $image, $imageName);
      $path = storage_path('app/public/'.$directory.'/thumbnail/'.$imageName);
      //$path = Storage::disk($directory)->get('thumbnail/'.$imageName);
      $resize = Image::make($image)->fit($this->size[$directory]['width'], $this->size[$directory]['height'])->save($path, 80);
      //Storage::disk($directory)->putFileAs('thumbnail', new File($path), $imageName);
      //Storage::put($path, $resize);
      $new_path = storage_path('app/public/'.$directory.'/thumbnail/'.$imageName);
      //Storage::disk($directory)->move($path, $new_path);
    }

    $imageUpload = new ImageUpload();
    $imageUpload->filename = $imageName;
    $imageUpload->save();
    return response()->json([ 'success' => $imageName ]);
  }

  public function fileDestroy(Request $request)
  {
    $filename =  $request->get('filename');
    ImageUpload::where('filename', $filename)->delete();
    $path = public_path().'/images/'.$filename;
    if(file_exists($path)) {
        unlink($path);
    }
    return $filename;
  }
}
