<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Subscriber;
use App\User;
use App\Trade;
use App\UserTrade;
use DB;
use Redirect;

class SubscriberController extends Controller
{
  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function index()
  {
    $authors = User::where('fk_role_id', 2)->where('status', 1)->get();
    $trade = Trade::where('status', 1)->get();
    $subscribers = Subscriber::where('fk_subscriber_id', Auth::user()->id)->get();
    $matrix = [ ];
    foreach($authors as $author)
      foreach($trade as $trade_row)
        foreach($trade_row->list as $trade_item)
        {
          $checked = Subscriber::where('fk_user_id', $author->id)->where('fk_trade_id', $trade_item->id)->where('fk_subscriber_id', Auth::user()->id)->first() ? true : false;
          $priceObj = UserTrade::where('fk_user_id', $author->id)->where('fk_trade_item_id', $trade_item->id)->first();
          $price = $priceObj ? $priceObj->price : 0;
          $matrix[$author->id][$trade_item->id] = [ 'checked' => $checked, 'price' => $price ];
        }

    return view('cabinet.subscribers', [ 'authors' => $authors, 'trade' => $trade, 'subscribers' => $subscribers, 'matrix' => $matrix ]);
  }

  /**
   * Save subscriber information
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function save(Request $request)
  {
    $trade = $request->trade;
    $authors = Array();
    foreach($trade as $author_id => $list)
    {
      $authors[] = $author_id;
      Subscriber::where('fk_user_id', $author_id)->where('fk_subscriber_id', Auth::user()->id)->delete();
      foreach($list as $item_id => $item)
      {
        Subscriber::create(['fk_user_id' => $author_id, 'fk_trade_id' => $item_id, 'fk_subscriber_id' => Auth::user()->id ]);
      }
    }
    Subscriber::whereNotIn('fk_user_id', $authors)->where('fk_subscriber_id', Auth::user()->id)->delete();

    return Redirect::to("profile/subscribers");
  }
}
