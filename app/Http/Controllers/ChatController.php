<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ChatDialog;
use App\ChatItem;
use App\Subscriber;
use App\User;
use Auth;

class ChatController extends Controller
{
  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function index()
  {
    $list = $this->getDialogList();

    return view('chat.chat', [ "list" => $list, "dialog" => "" ]);
  }

  private function getDialogList()
  {
    $list = ChatDialog::select("*")->where('status', 1)->where(function($query) {
        $query->where('fk_creator_id', Auth::user()->id)->orWhere('fk_reciever_id', Auth::user()->id);
      })->get();
    foreach($list as $item)
      $item->message = ChatItem::select('message', 'created_at')->where('status', '1')->where('fk_dialog_id', $item->id)->where(function($query) {
          $query->where('fk_sender_id', Auth::user()->id)->orWhere('fk_reciever_id', Auth::user()->id);
        })->orderBy('created_at', 'DESC')->take(1)->first();
    return $list;
  }

  private function getSubscribersList()
  {
    $list = Subscriber::select("*")->where('fk_user_id', Auth::user()->id)->groupBy("fk_subscriber_id")->get();

    return $list;
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function dialog($dialog_id)
  {
    $list = $this->getDialogList();

    return view('chat.chat', [ "list" => $list, "dialog" => $this->dialogChat($dialog_id) ]);
  }

  public function dialogChat($dialog_id)
  {
    $dialog = ChatDialog::select("*")->where('status', 1)->where('id', $dialog_id)->firstOrFail();

    $messages = ChatItem::select("*")->where('status', 1)->where('fk_dialog_id', $dialog_id)->get();
    if($dialog->fk_creator_id == Auth::user()->id)
      $reciever = User::where('status', 1)->where('id', $dialog->fk_reciever_id)->firstOrFail();
    else
      $reciever = User::where('status', 1)->where('id', $dialog->fk_creator_id)->firstOrFail();

    return view('chat.dialog', [ "messages" => $messages, "reciever" => $reciever, "dialog" => $dialog ]);
  }

  public function update(Request $request)
  {
    $dialog_id = $request->dialog_id;
    $last_message_id = $request->last_id;
    $dialog = ChatDialog::select("*")->where('status', 1)->where('id', $dialog_id)->firstOrFail();
    $messages = ChatItem::select("*")->where('status', 1)->where('fk_dialog_id', $dialog_id)->where('id', '>', $last_message_id)->orderBy('created_at')->get();
    if($dialog->fk_creator_id == Auth::user()->id)
      $reciever = User::where('status', 1)->where('id', $dialog->fk_reciever_id)->firstOrFail();
    else
      $reciever = User::where('status', 1)->where('id', $dialog->fk_creator_id)->firstOrFail();

    $html = "";
    $last_id = 0;
    foreach($messages as $message)
    {
      $html .= view('chat.message', [ "message" => $message, "reciever" => $reciever ]);
      $last_id = $message->id;
    }

    return json_encode(Array("list" => $html, "last_id" => $last_id));
  }

  public function subscribers()
  {
    $list = $this->getSubscribersList();

    return view('chat.subsсribers', [ "list" => $list, "dialog" => "" ]);
  }

  public function new($user_id)
  {
    $check = ChatDialog::where('status', 1)->where(function($query) use($user_id) {
        $query->where(function($query) use($user_id) {
          $query->where('fk_creator_id', Auth::user()->id)->where('fk_reciever_id', $user_id);
        })->orWhere(function($query) use($user_id) {
          $query->where('fk_reciever_id', Auth::user()->id)->where('fk_creator_id', $user_id);
        });
      })->first();
    if(is_object($check))
      $dialog_id = $check->id;
    else {
      $item = new ChatDialog;
      $item->fk_reciever_id = $user_id;
      $item->fk_creator_id = Auth::user()->id;
      $item->status = 1;
      $item->save();
      $dialog_id = $item->id;
    }
    return redirect('chat/'.$dialog_id);
  }

  public function add(Request $request)
  {
    $dialog_id= $request->dialog_id;
    $item = new ChatItem;
    $item->message = $request->message;
    $item->fk_reciever_id = $request->reciever;
    $item->fk_dialog_id = $request->dialog_id;
    $item->status = 1;
    $item->fk_sender_id = Auth::user()->id;
    $item->save();
    return $this->dialogChat($dialog_id);
  }
}
