<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CommentController extends Controller
{
  public function showBlogList($post_id)
  {
    $count = \App\Comment::where('status', 1)->where('fk_blog_item_id', $post_id)->count();

    return view('comment.blog_content', [ "count" => $count, "post_id" => $post_id, "comments_list" => $this->showList($post_id) ]);
  }

  public function showList($post_id)
  {
    $comments = \App\Comment::where('status', 1)->where('fk_blog_item_id', $post_id)->where('fk_comment_id', null)->orderBy('created_at', 'desc')->get();

    return view('comment.blog_list', [ "comments" => $comments, "post_id" => $post_id ]);
  }

  /**
   * Save profile information
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function store(Request $request)
  {
    $item = new \App\Comment;
    $message = $request->message;
    if(strpos($message, "@") === 0)
      $message = substr($message, strpos($message, " "));
    $item->message = $message;
    $item->fk_user_id = \Auth::user()->id;
    $item->status = 1;
    $item->fk_blog_item_id = $request->blog_id;
    if($request->comment_id > 0)
      $item->fk_comment_id = $request->comment_id;
    $item->save();
    return $this->showList($request->blog_id);
  }
}
