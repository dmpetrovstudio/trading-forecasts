<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ReferalController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      $authors = $this->getReferalUsersList(2);
      $pods = $this->getReferalUsersList(3);
      return view('cabinet.referal', [ "authors" => $authors, "pods" => $pods ]);
    }

    private function getReferalUsersList($role_id)
    {
      return User::select('*')
                ->where('users.status', '=', 1)
                ->where('users.fk_role_id', '=', $role_id)
                ->where('users.fk_referal_id', '=', \Auth::user()->id)
                ->get();
    }

    public function link(Request $request, $link)
    {
      $user = User::select("*")->where('referal', '=', $link)->first();
      if(isset($user->id))
      {
        $request->session()->put('referal_id', $user->id);
        $request->session()->put('referal_name', $user->name);
        return \Redirect::to("register");
      } else {
        return \Redirect::to("");
      }
    }
}
