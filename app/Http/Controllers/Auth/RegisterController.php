<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Subscriber;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserRegistered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/cabinet';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
      $referal = $this->generateReferal();

      $user = User::create([
          'name' => $data['name'],
          'email' => $data['email'],
          'referal' => $referal,
          'avatar' => 'default.jpg',
          'password' => Hash::make($data['password']),
          'fk_referal_id' => session('referal_id') > 0 ? session('referal_id') : null
      ]);
      $authors = isset($data['author']) ? $data['author'] : [];
      foreach($authors as $author_id => $trades)
      {
        if(is_array($trades) && sizeof($trades) > 0)
        {
          for($i = 0, $size = sizeof($trades); $i < $size; $i++)
          {
            Subscriber::create([
              'fk_user_id' => $author_id,
              'fk_trade_id' => $trades[$i],
              'fk_subscriber_id' => $user->id
            ]);
          }
        }
      }
      Mail::to($user)->send(new UserRegistered($user));
      return $user;
    }

    private function generateReferal()
    {
      $result = "";
      $symbol = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "a", "b", "c", "d", "e", "f", "g", "h",
  						"j", "i", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w",
  						"x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "J", "I", "K",
  						"L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
  		$count = sizeof($symbol);
  		for($i = 0; $i < 10; $i++)
  			$result .= $symbol[rand(0, $count-1)];
  		return $result;
    }

    public function showRegistrationForm()
    {
      $authors = User::select('users.*', 'users_trade.price', 'trade_item.name as trade_name')->from('users')
                          ->leftJoin('users_trade', 'users_trade.fk_user_id', '=', 'users.id')
                          ->leftJoin('trade_item', 'trade_item.id', '=', 'users_trade.fk_trade_item_id')
                          ->where('users.main_author', 1)->where('users.fk_role_id', 2)
                          ->groupBy('users.id')->take(6)->get();
      /*$author = User::select('select users.*, users_trade.price, trade_item.name as trade_name
                            from users
                            left join users_trade on users_trade.fk_user_id = users.id
                            left join trade_item on trade_item.id = users_trade.fk_trade_item_id
                            where users.main_author = ? AND users.fk_role_id = ?
                            GROUP BY users.id
                            limit 3',
                        array(1, 2));*/
      //echo "<pre>";print_r($author);echo "</pre>";exit();
      return view('auth.register', [ "authors" => $authors ]);
    }
}
