<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Intervention\Image\Facades\Image;
use Redirect;

class ImageController extends Controller
{
  private $size = Array("avatar" => Array("cabinet" => Array("width" => 160, "height" => 160),
                                          "icon" => Array("width" => 60, "height" => 60)),
                        "blog" => Array("post" => Array("width" => 993, "height" => 344),
                                        "list" => Array("width" => 561, "height" => 288),
                                        "icon" => Array("width" => 120, "height" => 120)),
                        "blogitem" => Array("post" => Array("width" => 561, "height" => 288),
                                          "icon" => Array("width" => 120, "height" => 120)));
    public function index($module, $rule, $image)
    {
      $path = storage_path('app/public/'.$module.'/'.$image);
      $save_path = storage_path('app/public/'.$module.'/'.$rule.'/'.$image);
      if(!is_file($save_path))
        Image::make($path)->fit($this->size[$module][$rule]['width'], $this->size[$module][$rule]['height'])->save($save_path);
      return Redirect::to('storage/'.$module.'/'.$rule.'/'.$image);
    }
}
