<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Trade;
use App\TradeItem;

class TradeController extends Controller
{
  public function index(Request $request)
  {
    $limit = 10;
    $list = Trade::where('status', 1);
    $list = $list->paginate($limit);

    return view('admin.trade.list', [ 'list' => $list ]);
  }

  public function showAdd()
  {
    return view('admin.trade.add', [ 'id' => 0 ]);
  }

  public function showAddChild($id)
  {
    return view('admin.trade.add', [ 'id' => $id ]);
  }

  public function add(Request $request)
  {
    if($request->parent > 0)
    {
      $trade = new TradeItem;
      $trade->name = $request->name;
      $trade->fk_trade_id = $request->parent;
      $trade->status = 1;
      $trade->save();
    } else {
      $trade = new Trade;
      $trade->name = $request->name;
      $trade->status = 1;
      $trade->save();
    }
    return redirect('admin/trade');
  }

  public function trash(Request $request, $id)
  {
    $trade = Trade::where('id', $id)->delete();
    return redirect('admin/trade');
  }

  public function trashItem(Request $request, $id)
  {
    $trade = TradeItem::where('id', $id)->delete();
    return redirect('admin/trade');
  }

  public function showEdit($id)
  {
    $item = Trade::where('id', $id)->firstOrFail();

    return view('admin.trade.edit', [ 'item' => $item ]);
  }

  public function showEditChild($id)
  {
    $item = TradeItem::where('id', $id)->firstOrFail();

    return view('admin.trade.editItem', [ 'item' => $item ]);
  }

  public function edit(Request $request)
  {
    $trade = Trade::where('id', $request->id)->firstOrFail();
    $trade->name = $request->name;
    $trade->save();
    return redirect('admin/trade');
  }

  public function editChild(Request $request)
  {
    $trade = TradeItem::where('id', $request->id)->firstOrFail();
    $trade->name = $request->name;
    $trade->save();
    return redirect('admin/trade');
  }
}
