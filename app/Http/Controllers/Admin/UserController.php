<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;

class UserController extends Controller
{
  public function index(Request $request)
  {
    $limit = 10;
    $list = User::where('status', 1);
    if($request->role)
      $list = $list->where('fk_role_id', $request->role);
    if($request->email)
      $list = $list->where('email', 'LIKE', '%'.$request->email.'%');
    if($request->name)
      $list = $list->where('name', 'LIKE', '%'.$request->name.'%');
    $list = $list->orderBy('created_at', 'desc')->paginate($limit);

    $roles = Role::where('status', 1)->get();

    return view('admin.users.list', [ 'list' => $list, 'roles' => $roles ]);
  }

  public function showEdit($id)
  {
    $item = User::where('id', $id)->firstOrFail();

    $roles = Role::where('status', 1)->get();

    return view('admin.users.edit', [ 'item' => $item, 'roles' => $roles ]);
  }

  public function edit(Request $request)
  {
    $trade = User::where('id', $request->id)->firstOrFail();
    $trade->name = $request->name;
    $trade->email = $request->email;
    $trade->main_author = $request->main_author == "on" ? 1 : 0;
    $trade->fk_role_id = $request->role;
    $trade->save();
    return redirect('admin/users');
  }

  public function trash(Request $request, $id)
  {
    $trade = User::where('id', $id)->delete();
    return redirect('admin/users');
  }

  public function hide(Request $request, $id)
  {
    $item = User::where('id', $request->id)->firstOrFail();
    $item->status = $item->status == 1 ? 2 : 1;
    $item->save();
    return redirect('admin/users');
  }

  public function passwordForm($id)
  {
    $item = User::where('id', $id)->firstOrFail();

    return view('admin.users.password', [ 'item' => $item ]);
  }

  public function password(Request $request)
  {
    $user = User::where('id', $request->id)->firstOrFail();
    $user->password = Hash::make($request->password);
    $user->save();
    return redirect('admin/users');
  }
}
