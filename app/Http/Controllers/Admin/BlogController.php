<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BlogItem;
use App\BlogType;
use App\User;
use Auth;

class BlogController extends Controller
{
  public function index(Request $request)
  {
    $limit = 10;
    $list = BlogItem::whereIn('status', [1,2]);
    if($request->title)
      $list = $list->where('title', 'like', '%'.$request->title.'%');
    if($request->type > 0)
      $list = $list->where('fk_blog_type_id', $request->type);
    if($request->trade > 0)
      $list = $list->where('fk_trade_id', $request->trade);
    if($request->author > 0)
      $list = $list->where('fk_user_id', $request->author);
    $list = $list->orderBy('created_at', 'desc')->paginate($limit);

    $trades = app('App\Http\Controllers\TradeController')->getTradeList();
    $types = BlogType::where('status', 1)->get();
    $authors = User::where('status', 1)->where('fk_role_id', 2)->get();

    return view('admin.blog.list', [ 'list' => $list, 'trades' => $trades, 'types' => $types, 'authors' => $authors ]);
  }

  public function showAdd()
  {
    $trades = app('App\Http\Controllers\TradeController')->getTradeList();
    $types = BlogType::where('status', 1)->get();

    return view('admin.blog.add', [ 'trades' => $trades, 'types' => $types ]);
  }

  public function add(Request $request)
  {
    $item = new BlogItem;
    $item->title = $request->title;
    $item->text = $request->text;
    $item->photo = $request->photo;
    $item->images = $request->images;
    $item->video = $request->video;
    $item->voice = $request->voice;
    $item->fk_user_id = Auth::user()->id;
    $item->fk_trade_id = $request->trade;
    $item->fk_blog_type_id = $request->type;
    $item->status = 1;
    $item->save();
    return redirect('admin/blog');
  }

  public function showEdit($id)
  {
    $item = BlogItem::where('id', $id)->firstOrFail();

    $trades = app('App\Http\Controllers\TradeController')->getTradeList();
    $types = BlogType::where('status', 1)->get();

    return view('admin.blog.edit', [ 'item' => $item, 'trades' => $trades, 'types' => $types ]);
  }

  public function edit(Request $request)
  {
    $item = BlogItem::where('id', $request->id)->firstOrFail();
    $item->title = $request->title;
    $item->text = $request->text;
    $item->fk_blog_type_id = $request->type;
    $item->fk_trade_id = $request->trade;
    $item->photo = $request->photo;
    $item->images = $request->images;
    $item->video = $request->video;
    $item->voice = $request->voice;
    $item->save();
    return redirect('admin/blog');
  }

  public function trash(Request $request, $id)
  {
    $trade = BlogItem::where('id', $id)->delete();
    return redirect('admin/blog');
  }

  public function hide(Request $request, $id)
  {
    $item = BlogItem::where('id', $request->id)->firstOrFail();
    $item->status = $item->status == 1 ? 2 : 1;
    $item->save();
    return redirect('admin/blog');
  }
}
