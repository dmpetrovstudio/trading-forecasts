<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Trade;
use App\UserTrade;
use Auth;
use Redirect;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      if(Auth::user()->fk_role_id == 2)
        return view('cabinet.profile_author');
      return view('cabinet.profile');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function info($name)
    {
      if($name == Auth::user()->name)
        return Redirect::to("profile");
      $user = User::where('name', $name)->firstOrFail();

      return view('cabinet.profile_info', [ "user" => $user ]);
    }

    public function price()
    {
      $user = User::where('id', Auth::user()->id)->firstOrFail();
      $list = Trade::where('status', 1)->get();
      /*$list = DB::table('trade_item')->
                        select('trade_item.*', 'trade.name as trade_name', 'users_trade.price as trade_price')->
                        leftJoin('trade', 'trade.id', '=', 'trade_item.fk_trade_id')->
                        leftJoin('users_trade', 'users_trade.fk_trade_item_id', '=', 'trade_item.id')->
                        where('trade_item.status', 1)->
                        where(function($query) {
                          $query->where('users_trade.fk_user_id', Auth::user()->id)->orWhereNull('users_trade.id');
                        })->
                        get();*/

      return view('cabinet.price', [ "user" => $user, "list" => $list ]);
    }

    public function priceSave(Request $request)
    {
      $data = $request->price;
      $frequency = $request->frequency;
      foreach($data as $trade_id => $price)
      {
        $inserted_id = DB::table('users_trade')->insertOrIgnore(['price' => $price, 'frequency' => $frequency[$trade_id], 'fk_user_id' => Auth::user()->id, 'fk_trade_item_id' => $trade_id]);
        if($inserted_id < 1)
          DB::table('users_trade')->where('fk_user_id', Auth::user()->id)->
                                  where('fk_trade_item_id', $trade_id)->
                                  update(['price' => $price, 'frequency' => $frequency[$trade_id]]);
      }
      return Redirect::to("profile-price");
    }

    /**
     * Save profile information
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function save(Request $request)
    {
      $user = \App\User::find(\Auth::user()->id);
      $user->name = $request->name;
      $user->email = $request->email;
      $user->description = $request->description;
      $user->birthday = $request->birthday;
      $user->country = $request->country;
      $user->city = $request->city;
      if(strlen($request->password) > 0 && $request->password == $request->password2)
      {
        $user->password = \Hash::make($request->password);
      }
      $user->save();
      return Redirect::to("profile");
    }

    public function loadAvatar(Request $request)
    {
      $avatar = $request->avatar;
      $user = User::find(Auth::user()->id);
      $user->avatar = $request->avatar;
      $user->save();
      return;
    }
}
