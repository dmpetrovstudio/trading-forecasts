<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BlogItem;
use App\TradeItem;

class TradeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      $list = $this->getTradeList();

      return view('trade.index', [ "list" => $list, "content" => "" ]);
    }

    public function getTradeList()
    {
      return \App\Trade::select("*")->where('status', '1')->get();
    }

    public function item(Request $request, $id)
    {
      $list = $this->getTradeList();
      $type = $request->type;
      $item = TradeItem::select("*")->where('status', '1')->where('id', $id)->firstOrFail();
      $blog = BlogItem::select("*")->where('status', '1')->where('fk_trade_id', $id);
      if($type > 0)
        $blog = $blog->where('fk_blog_type_id', $type);
      $blog = $blog->get();

      return view('trade.index', [ "list" => $list, "content" => view('trade.item', [ "list" => $list, "blog" => $blog, "item" => $item ]) ]);
    }
}
