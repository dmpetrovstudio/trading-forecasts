<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogItem extends Model
{
    /**
     * Таблица, связанная с моделью.
     *
     * @var string
     */
    protected $table = 'blog_item';

    public function user()
    {
        return $this->hasOne('\App\User', 'id', 'fk_user_id');
    }

    public function blogtype()
    {
        return $this->hasOne('\App\BlogType', 'id', 'fk_blog_type_id');
    }

    public function tradeitem()
    {
        return $this->hasOne('\App\TradeItem', 'id', 'fk_trade_id');
    }
}
