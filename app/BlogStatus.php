<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogStatus extends Model
{
    /**
     * Таблица, связанная с моделью.
     *
     * @var string
     */
    protected $table = 'blog_status';

    public $timestamps = false;
}
