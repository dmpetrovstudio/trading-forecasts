
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index')->name('main');

Auth::routes();

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/ref/{link}', 'ReferalController@link')->name('referal.link');
Route::get('/images/{module}/{rule}/{image}', 'ImageController@index')->name('images');

Route::group(['middleware' => ['admin']], function()
{
  Route::get('/admin', 'AdminController@index')->name('admin.index');

  Route::get('/admin/users', 'Admin\UserController@index')->name('admin.users');
  Route::get('/admin/users/trash/{id}', 'Admin\UserController@trash')->where('id', '[0-9]+')->name('admin.users.trash');
  Route::get('/admin/users/hide/{id}', 'Admin\UserController@hide')->where('id', '[0-9]+')->name('admin.users.hide');
  Route::get('/admin/users/edit/{id}', 'Admin\UserController@showEdit')->where('id', '[0-9]+')->name('admin.users.edit');
  Route::post('/admin/users/edit', 'Admin\UserController@edit')->where('id', '[0-9]+')->name('admin.users.editform');
  Route::get('/admin/users/password/{id}', 'Admin\UserController@passwordForm')->where('id', '[0-9]+')->name('admin.users.passwordForm');
  Route::post('/admin/users/password', 'Admin\UserController@password')->where('id', '[0-9]+')->name('admin.users.password');

  Route::get('/admin/blog', 'Admin\BlogController@index')->name('admin.blog');
  Route::get('/admin/blog/trash/{id}', 'Admin\BlogController@trash')->where('id', '[0-9]+')->name('admin.blog.trash');
  Route::get('/admin/blog/add', 'Admin\BlogController@showAdd')->name('admin.blog.add');
  Route::post('/admin/blog/add', 'Admin\BlogController@add')->name('admin.blog.addform');
  Route::get('/admin/blog/hide/{id}', 'Admin\BlogController@hide')->where('id', '[0-9]+')->name('admin.blog.hide');
  Route::get('/admin/blog/edit/{id}', 'Admin\BlogController@showEdit')->where('id', '[0-9]+')->name('admin.blog.edit');
  Route::post('/admin/blog/edit', 'Admin\BlogController@edit')->where('id', '[0-9]+')->name('admin.blog.editform');

  Route::get('/admin/trade', 'Admin\TradeController@index')->name('admin.trade');
  Route::get('/admin/trade/add', 'Admin\TradeController@showAdd')->name('admin.trade.add');
  Route::get('/admin/trade/add/{id}', 'Admin\TradeController@showAddChild')->where('id', '[0-9]+')->name('admin.trade.add');
  Route::post('/admin/trade/add', 'Admin\TradeController@add')->name('admin.trade.addform');
  Route::get('/admin/trade/trash/{id}', 'Admin\TradeController@trash')->where('id', '[0-9]+')->name('admin.trade.trash');
  Route::get('/admin/trade/trash-item/{id}', 'Admin\TradeController@trashItem')->where('id', '[0-9]+')->name('admin.trade.trash.item');
  Route::get('/admin/trade/edit/{id}', 'Admin\TradeController@showEdit')->where('id', '[0-9]+')->name('admin.trade.edit');
  Route::get('/admin/trade/edit-item/{id}', 'Admin\TradeController@showEditChild')->where('id', '[0-9]+')->name('admin.trade.edit.item');
  Route::post('/admin/trade/edit', 'Admin\TradeController@edit')->where('id', '[0-9]+')->name('admin.trade.editform');
  Route::post('/admin/trade/edit-item', 'Admin\TradeController@editChild')->where('id', '[0-9]+')->name('admin.trade.editform.item');
});
Route::group(['middleware' => ['auth']], function()
{
  Route::get('/cabinet', 'CabinetController@index')->name('cabinet');
  Route::get('/profile', 'ProfileController@index')->name('profile');
  Route::get('/profile/subscribers', 'SubscriberController@index')->name('profile.subscribers');
  Route::post('/profile/save-subscribers', 'SubscriberController@save')->name('subscriber.save');
  Route::post('/profile', 'ProfileController@save')->name('profile');
  Route::get('/profile/{name}', 'ProfileController@info')->where('name', '^[a-zA-Z0-9_]{1,}$')->name('profile.info');
  Route::get('/profile-price', 'ProfileController@price')->name('profile.price');
  Route::post('/profile-price/save', 'ProfileController@priceSave')->name('profile.price.save');
  Route::post('/profile/load-avatar', 'ProfileController@loadAvatar')->name('profile.load.avatar');
  Route::get('/referal', 'ReferalController@index')->name('referal');
  Route::get('/blog/add', 'BlogController@add')->name('blog');
  Route::post('/blog/add', 'BlogController@store')->name('blog.add');
  Route::get('/blog/my', 'BlogController@my')->name('blog');
  Route::get('/blog/last', 'BlogController@last')->name('blog');
  Route::get('/blog/item/{id}', 'BlogController@item')->where('id', '[0-9]+')->name('blog');
  Route::get('/blog/author/{author}', 'BlogController@authorList')->where('id', '^[a-zA-Z0-9_]{1,}$')->name('blog.authorList');
  Route::get('/chat', 'ChatController@index')->name('chat');
  Route::get('/chat/{dialog}', 'ChatController@dialog')->where('dialog', '[0-9]+')->name('chat');
  Route::post('/chat/add', 'ChatController@add')->name('chat');
  Route::get('/chat/new/{user_id}', 'ChatController@new')->where('user_id', '[0-9]+')->name('chat.new');
  Route::post('/chat/update', 'ChatController@update')->name('chat.update');
  Route::get('/subscribers', 'ChatController@subscribers')->name('subscribers');
  Route::get('/trade', 'TradeController@index')->name('trade');
  Route::get('/trade/{id}', 'TradeController@item')->name('trade_item');
  Route::post('/comment/add', 'CommentController@store')->name('comment.add');
  Route::get('/support', 'SupportController@index')->name('support.index');

  Route::post('image/upload','ImageUploadController@fileCreate');
  Route::post('image/upload/store','ImageUploadController@fileStore');
  Route::post('image/delete','ImageUploadController@fileDestroy');
});
