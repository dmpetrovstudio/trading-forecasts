registerForm = {
  Recalc: function() {
    var _tr = $('.subscription-table li.tr-author');
    var _all_amount = 0;
    for(var i = 0; i < _tr.length; i++)
    {
      var _amount = 0;
      var _list = $(_tr[i]).find('.subscription-table__market');
      for(var j = 0; j < _list.length; j++)
        _amount += Number($(_list[j]).data('price'));
      if(_amount == 0)
        $(_tr[i]).remove();
      $(_tr[i]).find('.td-amount strong').text(_amount + ' ₽');
      _all_amount += _amount;
    }
    $('.subscription-total__price').text('Сумма к оплате: ' + _all_amount + ' ₽');
    return true;
  },
  TableRebuild: function() {
    $('.subscription-table .tr-author').remove();
    var _authors = $('.subscription-markets');
    for(var i = 0; i < _authors.length; i++)
    {
      var _author_id = $(_authors[i]).data('author');
      var _author_name = $(_authors[i]).data('name');
      var oLi = $('<li>').addClass('tr tr-author author-' + _author_id);
      $('<input type="hidden" name="author[]" class="author-' + _author_id + '" value="' + _author_id + '">').appendTo(oLi);
      $('<span class="td"><strong>' + _author_name + '</strong></span>').appendTo(oLi);
      var oSpan = $('<span class="td"></span>');
      var checkedList = $(_authors[i]).find('[type=checkbox]:checked');
      for(var j = 0; j < checkedList.length; j++)
      {
        var _user = Number($(checkedList[j]).data('user'));
        var _trade = Number($(checkedList[j]).data('trade'));
        var _price = Number($(checkedList[j]).data('price'));
        var _name = $(checkedList[j]).data('name');
        if(_trade == 0)
        {
          var checkedSubList = $(checkedList[j]).parents('.check-list').find('[type=checkbox][data-trade!="0"]');
          for(var k = 0; k < checkedSubList.length; k++)
          {
            var _user = Number($(checkedSubList[k]).data('user'));
            var _trade = Number($(checkedSubList[k]).data('trade'));
            var _price = Number($(checkedSubList[k]).data('price'));
            var _name = $(checkedSubList[k]).data('name');
            $('<span class="subscription-table__market trade-item-' + _user + '-' + _trade + '" data-id="' + _trade + '" data-price="' + _price + '">' + _name + ' <a href="#" class="btn-clear btn-remove-trade-item" data-item="' + _trade + '" data-author="' + _user + '"><i class="icon-clear"></i></a></span>').appendTo(oSpan);
            $('<input type="hidden" name="author[' + _user + '][]" class="trade-item-' + _user + '-' + _trade + '" value="' + _trade + '">').appendTo(oSpan);
          }
        } else {
          $('<span class="subscription-table__market trade-item-' + _user + '-' + _trade + '" data-id="' + _trade + '" data-price="' + _price + '">' + _name + ' <a href="#" class="btn-clear btn-remove-trade-item" data-item="' + _trade + '" data-author="' + _user + '"><i class="icon-clear"></i></a></span>').appendTo(oSpan);
          $('<input type="hidden" name="author[' + _user + '][]" class="trade-item-' + _user + '-' + _trade + '" value="' + _trade + '">').appendTo(oSpan);
        }
      }
      $(oSpan).appendTo(oLi);
      $('<span class="td td-amount"><strong>0 ₽</strong></span>').appendTo(oLi);
      $('<span class="td"><a href="#" class="btn-clear btn-remove-subscriber" data-author="' + _author_id + '"><i class="icon-clear"></i></a></span>').appendTo(oLi);
      $('.subscription-table').append(oLi);
    }
    return true;
  }
};

var CRegisterForm = registerForm;

$(document).ready(function() {
  $(document).on('click', '.btn-remove-trade-item', function() {
    var _item = $(this).data('item');
    var _author = $(this).data('author');
    $('.trade-item-' + _author + '-' + _item).remove();
    $('.subscription-item [type=checkbox][data-trade="' + _item + '"][data-user="' + _author + '"]')[0].checked = false;
    CRegisterForm.Recalc();
    return false;
  });
  $(document).on('click', '.btn-remove-subscriber', function() {
    var _author = $(this).data('author');
    $('.author-' + _author).remove();
    CRegisterForm.Recalc();
    return false;
  });
  $(document).on('change', '.subscription-markets [type=checkbox]', function() {
    var _trade = Number($(this).data('trade'));
    if(_trade == 0)
    {
      var _items = $(this).parents('.check-list').find('[type=checkbox][data-trade!=0]');
      for(var i = 0; i < _items.length; i++)
        _items[i].checked = false;
    } else {
      $(this).parents('.check-list').find('[type=checkbox][data-trade=0]')[0].checked = false;
    }
    CRegisterForm.TableRebuild();
    CRegisterForm.Recalc();
  });
});
