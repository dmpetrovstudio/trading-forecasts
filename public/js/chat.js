function Chat()
{

}
Chat.prototype.Add = function() {
  $.post('/chat/add', $('.chat-form').serialize(), function(_result) {
    $('#chat').html(_result);
    $('.scrollbar-inner').scrollbar({
      ignoreMobile: true
    });
    $('#chat .scroll-content').scrollTop($('#chat .scroll-content')[0].scrollHeight);
    $('.chat-form textarea').focus();
  });
};
Chat.prototype.Update = function() {
  $.post('/chat/update', $('.chat-form').serialize(), function(_result) {
    $('#chat .scroll-content').append(_result['list']);
    $('#chat .scroll-content').scrollTop($('#chat .scroll-content')[0].scrollHeight);
    if(_result['last_id'] > 0)
    $('.chat-form [name=last_id]').val(_result['last_id']);
  }, 'json');
  setTimeout(Chat.Update, 5000);
};
var Chat = new Chat();

$(document).ready(function() {
  if($('#chat .scroll-content').length > 0)
  {
    $('#chat .scroll-content').scrollTop($('#chat .scroll-content')[0].scrollHeight);
    $('.chat-form textarea').focus();
  }

  $(document).on('submit', '.chat-form', function() {
    Chat.Add();
    return false;
  });
  $(document).on('keyup', '.chat-form textarea', function(e) {
    if (e.ctrlKey && e.keyCode == 13) {
      Chat.Add();
    }
  });
  Chat.Update();
});
