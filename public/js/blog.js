$(document).ready(function() {
  $(document).on('click', '.blog-tabs a', function() {
    var _tab = $(this).data('tab');
    $('.blog-tabs a').removeClass('active');
    $(this).addClass('active');
    $('.blog-form-content').css('display', 'none');
    $('.blog-form-content[data-tab="' + _tab + '"]').fadeIn();
    return false;
  });
  $(document).on('click', '#blogAddSubmit', function() {
    $('#blogAddForm').submit();
    return false;
  });
  if($('.add-text__text').length > 0)
    CKEDITOR.replace('add-text__text', {
      toolbar: [
              { name: 'document', items: [ 'NewPage', 'Preview', '-', 'Templates' ] },
              { name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
              { name: 'basicstyles', items: [ 'Font','FontSize','Bold','Italic','Underline','Strike','Subscript','Superscript'] },
	            { name: 'colors', items : [ 'TextColor' ] },
          ]
    });
  Dropzone.options.dropzonePhoto =
	{
    maxFiles: 1,
		maxFilesize: 3,
		renameFile: function(file) {
				var dt = new Date();
				var time = dt.getTime();
			 return time+file.name;
		},
		acceptedFiles: ".jpeg,.jpg,.png,.gif",
		addRemoveLinks: true,
		timeout: 5000,
		dictDefaultMessage: '<span class="add-image__icon dz-message"><i class="icon-image"></i><strong class="hidden-xs">Нажмите или перетащите чтобы добавить изображение</strong><strong class="visible-xs">Загрузить изображение</strong></span>',
		init: function(){
       //$(this.element).html($(this.element).html() + this.options.dictDefaultMessage);
    },
    processing: function()
    {
    	$(this.element).find('.dz-message').remove();
    },
		success: function(file, response)
		{
			console.log(response);
			if(response.success)
			{
				var _photo = $('#blogAddForm [name=photo]').val();
				if(_photo.length > 0)
					$('#blogAddForm [name=photo]').val(_photo + ',' + response.success);
				else
					$('#blogAddForm [name=photo]').val(response.success);
			}
		},
		error: function(file, response)
		{
       console.log(response);
       alert(response.message);
			 return false;
		},
		removedfile: function(file)
		{
				var name = file.upload.filename;
				$.ajax({
						headers: {
												'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
										},
						type: 'POST',
						url: '{{ url("image/delete") }}',
						data: {filename: name},
						success: function (data){
								console.log("File has been successfully removed!!");
						},
						error: function(e) {
								console.log(e);
						}});
						var fileRef;
						return (fileRef = file.previewElement) != null ?
						fileRef.parentNode.removeChild(file.previewElement) : void 0;
		},
	};
	Dropzone.options.dropzoneImages =
	 {
			maxFilesize: 3,
			renameFile: function(file) {
					var dt = new Date();
					var time = dt.getTime();
				 return time+file.name;
			},
			acceptedFiles: ".jpeg,.jpg,.png,.gif",
			addRemoveLinks: true,
			timeout: 5000,
			dictDefaultMessage: '<span class="add-image__icon dz-message"><i class="icon-image"></i><strong class="hidden-xs">Нажмите или перетащите чтобы добавить изображение</strong><strong class="visible-xs">Загрузить изображение</strong></span>',
			init: function(){
	       //$(this.element).html(this.options.dictDefaultMessage);
	    },
      processing: function()
      {
      	$(this.element).find('.dz-message').remove();
      },
			success: function(file, response)
			{
				console.log(response);
				if(response.success)
				{
					var _photo = $('#blogAddForm [name=images]').val();
					if(_photo.length > 0)
						$('#blogAddForm [name=images]').val(_photo + ',' + response.success);
					else
						$('#blogAddForm [name=images]').val(response.success);
				}
			},
			error: function(file, response)
			{
         console.log(response);
         alert(response.message);
				 return false;
			},
			removedfile: function(file)
			{
					var name = file.upload.filename;
					$.ajax({
							headers: {
													'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
											},
							type: 'POST',
							url: '{{ url("image/delete") }}',
							data: {filename: name},
							success: function (data){
									console.log("File has been successfully removed!!");
							},
							error: function(e) {
									console.log(e);
							}});
							var fileRef;
							return (fileRef = file.previewElement) != null ?
							fileRef.parentNode.removeChild(file.previewElement) : void 0;
			},
		};
		Dropzone.options.dropzoneVideo =
		 {
				maxFilesize: 50,
				renameFile: function(file) {
						var dt = new Date();
						var time = dt.getTime();
					 return time+file.name;
				},
				acceptedFiles: ".mp4",
				addRemoveLinks: true,
				timeout: 5000,
				dictDefaultMessage: '<span class="add-image__icon dz-message"><i class="icon-image"></i><strong class="hidden-xs">Нажмите или перетащите чтобы добавить видеофайл</strong><strong class="visible-xs">Загрузить видеофайл</strong></span>',
				init: function(){
		       //$(this.element).html(this.options.dictDefaultMessage);
		    },
	      processing: function()
	      {
	      	$(this.element).find('.dz-message').remove();
	      },
				success: function(file, response)
				{
					console.log(response);
					if(response.success)
					{
						var _photo = $('#blogAddForm [name=video]').val();
						if(_photo.length > 0)
							$('#blogAddForm [name=video]').val(_photo + ',' + response.success);
						else
							$('#blogAddForm [name=video]').val(response.success);
					}
				},
				error: function(file, response)
				{
          console.log(response);
          alert(response.message);
					return false;
				},
				removedfile: function(file)
				{
						var name = file.upload.filename;
						$.ajax({
								headers: {
														'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
												},
								type: 'POST',
								url: '{{ url("image/delete") }}',
								data: {filename: name},
								success: function (data){
										console.log("File has been successfully removed!!");
								},
								error: function(e) {
										console.log(e);
								}});
								var fileRef;
								return (fileRef = file.previewElement) != null ?
								fileRef.parentNode.removeChild(file.previewElement) : void 0;
				},
			};
			Dropzone.options.dropzoneVoice =
			 {
					maxFilesize: 10,
					renameFile: function(file) {
							var dt = new Date();
							var time = dt.getTime();
						 return time+file.name;
					},
					acceptedFiles: ".mp3",
					addRemoveLinks: true,
					timeout: 5000,
					dictDefaultMessage: '<span class="add-image__icon dz-message"><i class="icon-image"></i><strong class="hidden-xs">Нажмите или перетащите чтобы добавить аудиофайл</strong><strong class="visible-xs">Загрузить аудиофайл</strong></span>',
					init: function(){
			       //$(this.element).html(this.options.dictDefaultMessage);
			    },
		      processing: function()
		      {
		      	$(this.element).find('.dz-message').remove();
		      },
					success: function(file, response)
					{
						console.log(response);
						if(response.success)
						{
							var _photo = $('#blogAddForm [name=voice]').val();
							if(_photo.length > 0)
								$('#blogAddForm [name=voice]').val(_photo + ',' + response.success);
							else
								$('#blogAddForm [name=voice]').val(response.success);
						}
					},
					error: function(file, response)
					{
            console.log(response);
            alert(response.message);
						return false;
					},
					removedfile: function(file)
					{
							var name = file.upload.filename;
							$.ajax({
									headers: {
															'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
													},
									type: 'POST',
									url: '{{ url("image/delete") }}',
									data: {filename: name},
									success: function (data){
											console.log("File has been successfully removed!!");
									},
									error: function(e) {
											console.log(e);
									}});
									var fileRef;
									return (fileRef = file.previewElement) != null ?
									fileRef.parentNode.removeChild(file.previewElement) : void 0;
					},
				};
});
