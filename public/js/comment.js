function Comment()
{

}
Comment.prototype.Add = function() {
  $.post('/comment/add', $('.comments-form').serialize(), function(_result) {
    $('.comments-block').html(_result);
    $('.comments-form textarea').val('');
    $('.comments h3 span').text(Number($('.comments h3 span').text()) + 1);
  });
};
var Comment = new Comment();

$(document).ready(function() {
  $(document).on('submit', '.comments-form', function() {
    Comment.Add();
    return false;
  });
  $(document).on('keyup', '.comments-form textarea', function(e) {
    if (e.ctrlKey && e.keyCode == 13) {
      Comment.Add();
    }
  });
  $(document).on('click', '.comments-item__reply', function() {
    $('.comments-form [name=comment_id]').val($(this).data('id'));
    $('.comments-form textarea').val("@" + $(this).data('author') + " ");
    $('.comments-form textarea').focus();
    return false;
  });
});
