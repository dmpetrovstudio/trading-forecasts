var dropzone=null;

$(document).ready(function() {
  $(document).on('click', '.reset-password-btn', function() {
    $('.password-form').toggle();
    return false;
  });
  $(document).on('click', '.referal_copy', function() {
    clipboard.writeText($('#referal_url').val());
    return false;
  });
  $(document).on('submit', '#subscribersProfile', function() {
    var _list = $('#subscribersProfile').find('tr');
    var _data = '_token=' + $('#subscribersProfile').find('[name=_token]').val();
    for(var i = 0; i < _list.length; i++)
    {
      if(Number($(_list[i]).data('author')) > 0)
      {
        var _trades = $(_list[i]).find('.subscription-table__market');
        _data = _data + "&author[]=" + $(_list[i]).data('author');
        for(var j = 0; j < _trades.length; j++)
        {
          _data = _data + "&trades[" + $(_list[i]).data('author') + "][]=" + $(_trades[j]).data('id');
        }
      }
    }
    $.post('/profile/save-subscribers', _data, function(_result) {
      $('#answer-out').html(_result);
    });
    return false;
  });
  $(document).on('click', '.btn-subscriber-delete', function() {
    $(this).parents('tr').remove();
    return false;
  });
  $(document).on('click', '.btn-remove-trade-item', function() {
    $(this).parent().remove();
    return false;
  });
  dropzone = Dropzone.options.avatarForm =
	{
		maxFilesize: 3,
		renameFile: function(file) {
			 var dt = new Date();
			 var time = dt.getTime();
		   return time+file.name;
		},
		acceptedFiles: ".jpeg,.jpg,.png,.gif",
		addRemoveLinks: true,
    clickable: '.dropzone img',
		timeout: 5000,
		dictDefaultMessage: '',
    previewTemplate: '<label for="avatarForm"><img data-dz-thumbnail></label>',
		init: function(){
       //$(this.element).html($(this.element).html() + this.options.dictDefaultMessage);
    },
    processing: function()
    {
    	$(this.element).find('.dz-message').remove();
    },
		success: function(file, response)
		{
			console.log(response.success);
			if(response.success)
			{
        $($('#avatarForm > label')[0]).remove();
        $.post('/profile/load-avatar', { _token: $('#avatarForm [name="_token"]').val(), avatar: response.success });
				$('#avatarForm [name=avatar]').val(response.success);
			}
		},
		error: function(file, response)
		{
       console.log(response);
       alert(response.message);
			 return false;
		},
		removedfile: function(file)
		{
				var name = file.upload.filename;
				$.ajax({
						headers: {
												'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
										},
						type: 'POST',
						url: '{{ url("image/delete") }}',
						data: {filename: name},
						success: function (data){
								console.log("File has been successfully removed!!");
						},
						error: function(e) {
								console.log(e);
						}});
						var fileRef;
						return (fileRef = file.previewElement) != null ?
						fileRef.parentNode.removeChild(file.previewElement) : void 0;
		},
	};
});
